from __future__ import annotations

from . import nev_raw

class Design:
    def __init__(self, _design: nev_raw.RawDesign):
        self._design = _design

    def __len__(self):
        return self._design.module_count

    def __getitem__(self, module_name: str) -> Module:
        mod_id = self._design.get_module_id(module_name)

        if mod_id is None:
            raise KeyError(module_name)

        return Module(self._design, mod_id)

    # list of (mod_id, name)
    def get_name_map(self) -> list[tuple[int, str]]:
        return self._design.get_module_name_map()

    @classmethod
    def from_source(cls, source: str, opt_level=0):
        _design = nev_raw.compile(source, opt_level)
        return Design(_design)

class Module:
    def __init__(self, _design, mod_id):
        self._design = _design
        self.mod_id = mod_id

    @property
    def design(self):
        return Design(self._design)

    @property
    def graph_repr(self) -> str:
        return self._design.module_get_graph_debug_text(self.mod_id)

    @property
    def runtime_graph_repr(self) -> str:
        return self._design.module_get_compact_graph_debug_text(self.mod_id)

    @property
    def input_count(self) -> int:
        return self._design.module_get_input_count(self.mod_id)

    @property
    def output_count(self) -> int:
        return self._design.module_get_output_count(self.mod_id)

    def stat_cell(self) -> dict[str, int]:
        raw_cell_stat = self._design.module_get_raw_cell_stat(self.mod_id)
        name_map = dict(self.design.get_name_map())

        cell_stat = {}
        for (name, count) in raw_cell_stat.items():
            if name.startswith('$_'):
                continue

            if name.startswith("M_"):
                print(name)
                mod_id = int(name[2:])
                name = name_map[mod_id]

            cell_stat[name] = count
        
        return cell_stat

    def eval(self, inputs: list[int]) -> list[int]:
        return self._design.module_eval(self.mod_id, inputs)
