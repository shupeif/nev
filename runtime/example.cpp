#include "nev_rt_support.h"

struct entity_FA {
    uint32_t A;
    uint32_t B;
    uint32_t CI;
    uint32_t S;
    uint32_t CO;
    uint32_t W1;
    uint32_t W2;
    uint32_t W3;

    void execute_comb() {
        W1 = A ^ B;
        W2 = A & B;
        W3 = W1 & CI;
        S = W1 ^ CI;
        CO = W2 | W3;
    }

    void set_input(nev_port_id port, signal_t value) {
        switch (port) {
        case 0:
            A = !!value;
            break;
        case 1:
            B = !!value;
            break;
        case 2:
            CI = !!value;
            break;
        }
    }
    signal_t get_output(nev_port_id port) const {
        switch (port) {
        case 3:
            return S;
        case 4:
            return CO;
        }
        return (signal_t)(-1);
    }
    signal_t peek_signal(nev_signal_id sig) const {
        switch (sig) {
        case 0:
            return A;
        case 1:
            return B;
        case 2:
            return CI;
        case 3:
            return S;
        case 4:
            return CO;
        case 5:
            return W1;
        case 6:
            return W2;
        case 7:
            return W3;
        }
        return (signal_t)(-1);
    }
};

static const nev_entity_info ENTIY_INFO_FA = {

};

static const nev_entity_info* ENTITY_LIST[] = {
    &ENTIY_INFO_FA,
};

static const nev_metadata METADATA = {
    .version = 1,
    .min_runtime_version = 1,
    .entities = ENTITY_LIST,
    .entity_count = 1,
};

const nev_metadata* nev_get_metadata_v0(void) {
    return &METADATA;
}

const entity_vtable* get_entity_vtable(nev_entity_id entity) {
    switch (entity) {
    case 0:
        return &entity_vtable_trait<entity_FA>::METADATA;
    default:
        return nullptr;
    }
}