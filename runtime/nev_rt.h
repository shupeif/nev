#ifndef _NEV_RT_H_
#define _NEV_RT_H_

#include <stddef.h>
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

struct nev_string {
    const char* ptr;
    size_t len;
};

// valid port id is always non negative
typedef int32_t nev_port_id;

// valid signal is is always non negative
// signal id is permitted to be the same with port id
// but not enforced
typedef int32_t nev_signal_id;

typedef int32_t nev_entity_id;


typedef uint32_t signal_t;

struct nev_entity_info {
    nev_entity_id id;
    nev_string name;
};

struct nev_metadata {
    uint32_t version;
    uint32_t min_runtime_version;
    const nev_entity_info** entities;
    size_t entity_count;
};

struct entity_vtable {
    size_t size;
    size_t alignment;
    void (*init)(void* storage);
    void (*deinit)(void* storage);
    void (*execute_comb)(void* _entity);
    signal_t (*peek_signal)(const void* _entity, nev_signal_id sig);
    signal_t (*get_output)(const void* _entity, nev_port_id port);
    void (*set_input)(void* _entity, nev_port_id port, signal_t value);
};

typedef const nev_metadata* (*PFN_GET_METADATA_V0)(void);

__attribute__((visibility("default")))
const nev_metadata* nev_get_metadata_v0(void);

__attribute__((visibility("default")))
const nev_metadata* nev_get_entity_vtable_v0(void);

#ifdef __cplusplus
}
#endif

#endif /* _NEV_RT_H_ */
