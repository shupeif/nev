#include "nev_rt.h"
#include <new>

template<typename T>
struct entity_vtable_trait {
    static void init(void* storage) {
        T* entity = static_cast<T*>(storage);
        new(entity) T;
    }
    static void deinit(void* storage) {
        T* entity = static_cast<T*>(storage);
        entity->~T();
    }
    static void execute_comb(void* _entity) {
        T* entity = static_cast<T*>(_entity);
        return entity->execute_comb();
    }
    static void set_input(void* _entity, nev_port_id port, signal_t value) {
        T* entity = static_cast<T*>(_entity);
        return entity->set_input(port, value);
    }
    static signal_t peek_signal(const void* _entity, nev_signal_id sig) {
        const T* entity = static_cast<const T*>(_entity);
        return entity->peek_signal(sig);
    }
    static signal_t get_output(const void* _entity, nev_port_id port) {
        const T* entity = static_cast<const T*>(_entity);
        return entity->get_output(port);
    }
    static const entity_vtable METADATA;
};

template<typename T>
const entity_vtable entity_vtable_trait<T>::METADATA = {
    .size = sizeof(T),
    .alignment = alignof(T),
    .init = init,
    .deinit = deinit,
    .execute_comb = execute_comb,
    .peek_signal = peek_signal,
    .get_output = get_output,
    .set_input = set_input,
};