# Nev

Nev is a hadrware design language for RTL and gate level design and verification.

## License

Nev compiler is dual licensed under MIT and Apache 2.0.

Examples and test cases are additional licensed under CC0.