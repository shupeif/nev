# Nev Language Brief Introduction

The top level construct in Nev is `design`. A design is just a collection of `module`s. 
Each module can declare input and output `port`s, as the module's interface.
Additionally, module can have internal `wire`s and instantiate other modules as `cell`s.
Sometimes we may not need to distinguish ports from internal wires, thus they coule be collectively called `signal`s.

## Simple Combinational Module

Here's an example how to construct FA (full adder) in Nev.

```
module FA {
    input  A, B;
    input  CI;
    output S;
    output CO;

    wire 
}
```

A module has a name, and has a collection of `statement`s as its body.
Here we encounter two kinds of statements: signal declaration statements, and connection statements.

### Signal declaration statements

Signal declaration statements declare input ports, output ports, or internal wires.
Each statement could declare one or mutiple signals.
All signals must be declared explicity. There's no Verilog-style implicitly defined wires.

Unlike Verilog, Ports are declared in module's body and are not required to repeat in module's header. 

Order of statements in a module is not important. That says, port declartion could be put anywhere in the module,
and signals could be used before declaration (but it must be declared somewhere).

If statements in a module are ordered chaotically, it could be a disaster for readers.
Generally we have the following styling rules: 
- Port declarations should be put at the beginning of a module.
- Signals are recommended to be decla### Expression
red before the first use
- Connections are recommended to be ordered reflecting the dataflow.

These recommendations are soft. Ocassionally, slightly breaking the rules may improve readability.

<!--
Some may argue for addtional styling checks, such as declare one signal per line.
Generally we suggest that readability should be preferred than strictly conforming to specific style rules.
-->

### Connection statements

Connection statements have the form `lhs := rhs ;`. Lhs should be a signal and rhs should be a expression.

Connection statements could be regarded as a complex cell.
Its output pin is connected to lhs and its input pins are connected to signals occurred in rhs.
Lhs is said to _combinitional depend_ on signals occurred in rhs.

**NOTE**: the complete rule for combinational dependence analysis is complicated.
Here is just for the simplest case. We will expand it later step by step.

Combinational dependence is a core concept in both digital design and Nev.
Combinational dependence amang signals forms _combinational dependence graph_,
where vertices are signals and a edge from signal A to signal B represents that B depend on A.

### Well-formedness Check

Comforming to all these syntactic rules is _not_ enough to make sure the circuit is well-behaved.
Therefoce, Nev performs additional semantic checks:
- Each signal must have exactly one driver. Input ports are regarded as having one implicit driver outside the module.
- The combinational dependence graph of  an DAG. (a.k.a. combinitional loops are forbidden.)

Currently the two checks are strictly enforced. It may be relaxed in the future.
Floating signals are forbidden even if they are used to drive other logics.
Similarly, combinational loops are forbidden even if they are not connected to output ports.

## Hierarchical Combinational Module

Here's another example of FA. Notice that FA not only contains simple cells, but also instantiate other modules.

```
```

### Well-formedness Check

Since we introduce cells, one driver rule and no combloop rule should be expanded accordingly.

- Modules can not be instantiated recursively, neither directly nor indirectly.
- Like other signals, Input pins of cells should have exactly one driver.
  The output pins of cell is implicitly driven by the cell.
- For each cell, its output pins are combinationally depend on all of its input pins.

**NOTE**: here combinational dependence analysis is performed in a conservative modular way.
The word 'modular' means that we can not inspect a cell's internal structure.
The work 'conservative' means that we must conservatively assume all output pins depend on all input pins
since the cell is opaque.

## Expression

Expression preference

## Comment

Nev use C style comments. `//` indicates the beginning of line comments. `/* */` represents block comments.
Nested block comments is supported in Nev.

## Type

Currently Nev has only one type `bit`. It represents a two-value logic, 0 or 1.
Since we have only one type, all signals are implicitly typed with bit.
There is no syntactic construct referring to type.