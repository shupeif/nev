# Grammar

```
Design  = PkgItem*

PkgItem = Module
        | Item

Module  =
  Attr?
  "module" name@Ident
  "{" ModItem* "}"

ModItem =
  | ("input" | "output" | "wire")
    name@ident
    (":" ty@Ty)?
    ";"  
  | Expr ":=" Expr
    ";"

Ty      = Expr
Expr =
  | ident
  | literal_number
  | literal_bit
  | "(" Expr ")"
```