from pathlib import Path

import nev

def compile_module(top, filename):
    cur_dir = Path(__file__).parent.resolve()
    with open(cur_dir / 'cases' / filename) as f:
        source = f.read()
    return nev.Design.from_source(source)[top]

def test_constant():
    m = compile_module('CONST', '0001-CONST.nev')

    assert m.eval([]) == [0, 1]

    assert m.stat_cell() == {}

def test_buf():
    m = compile_module('BUF', '0002-BUF.nev')

    assert m.eval([0]) == [0]
    assert m.eval([1]) == [1]

    assert m.stat_cell() == {}

def test_not():
    m = compile_module('NOT', '0003-NOT.nev')

    assert m.eval([0]) == [1]
    assert m.eval([1]) == [0]

    assert m.stat_cell() == { '$not': 1 }

def test_and():
    m = compile_module('AND', '0004-AND.nev')

    assert m.eval([0, 0]) == [0]
    assert m.eval([0, 1]) == [0]
    assert m.eval([1, 0]) == [0]
    assert m.eval([1, 1]) == [1]

    assert m.stat_cell() == { '$and': 1 }

def test_or():
    m = compile_module('OR', '0005-OR.nev')

    assert m.eval([0, 0]) == [0]
    assert m.eval([0, 1]) == [1]
    assert m.eval([1, 0]) == [1]
    assert m.eval([1, 1]) == [1]

    assert m.stat_cell() == { '$or': 1 }

def test_xor():
    m = compile_module('XOR', '0006-XOR.nev')

    assert m.eval([0, 0]) == [0]
    assert m.eval([0, 1]) == [1]
    assert m.eval([1, 0]) == [1]
    assert m.eval([1, 1]) == [0]

    assert m.stat_cell() == { '$xor': 1 }

def test_nand():
    m = compile_module('NAND', '0007-NAND.nev')

    assert m.eval([0, 0]) == [1]
    assert m.eval([0, 1]) == [1]
    assert m.eval([1, 0]) == [1]
    assert m.eval([1, 1]) == [0]

    assert m.stat_cell() == { '$nand': 1 }

def test_nor():
    m = compile_module('NOR', '0008-NOR.nev')

    assert m.eval([0, 0]) == [1]
    assert m.eval([0, 1]) == [0]
    assert m.eval([1, 0]) == [0]
    assert m.eval([1, 1]) == [0]

    assert m.stat_cell() == { '$nor': 1 }

def test_xnor():
    m = compile_module('XNOR', '0009-XNOR.nev')

    assert m.eval([0, 0]) == [1]
    assert m.eval([0, 1]) == [0]
    assert m.eval([1, 0]) == [0]
    assert m.eval([1, 1]) == [1]

    assert m.stat_cell() == { '$xnor': 1 }

def test_mux():
    m = compile_module('MUX', '0010-MUX.nev')

    assert m.eval([0, 0, 0]) == [0]
    assert m.eval([0, 0, 1]) == [1]
    assert m.eval([0, 1, 0]) == [0]
    assert m.eval([0, 1, 1]) == [1]
    assert m.eval([1, 0, 0]) == [0]
    assert m.eval([1, 0, 1]) == [0]
    assert m.eval([1, 1, 0]) == [1]
    assert m.eval([1, 1, 1]) == [1]

    assert m.stat_cell() == { '$mux': 1 }