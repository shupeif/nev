from pathlib import Path

from nev import Design

def compile_module(top, filename):
    cur_dir = Path(__file__).parent.resolve()
    with open(cur_dir / 'cases' / filename) as f:
        source = f.read()
    return Design.from_source(source)[top]

def test_ha():
    m = compile_module('HA', '0100-MODULE.nev')

    assert m.eval([0, 0]) == [0, 0]
    assert m.eval([0 ,1]) == [0, 1]
    assert m.eval([1, 0]) == [0, 1]
    assert m.eval([1 ,1]) == [1, 0]

    assert m.stat_cell() == { '$xor': 1, '$and': 1 }

def test_fa():
    m = compile_module('FA', '0100-MODULE.nev')

    assert m.eval([0, 0, 0]) == [0, 0]
    assert m.eval([0, 0 ,1]) == [0, 1]
    assert m.eval([0, 1, 0]) == [0, 1]
    assert m.eval([0, 1 ,1]) == [1, 0]
    assert m.eval([1, 0, 0]) == [0, 1]
    assert m.eval([1, 0 ,1]) == [1, 0]
    assert m.eval([1, 1, 0]) == [1, 0]
    assert m.eval([1, 1 ,1]) == [1, 1]

    assert m.stat_cell() == { 'HA': 2, '$or': 1 }
    