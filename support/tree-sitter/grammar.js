module.exports = grammar({
  name: 'nev',
  
  rules: {
    // TODO: add the actual grammar rules
    source_file: $ => repeat($._item),

    _item: $ => choice(
      $.module_item,
    ),

    module_item: $ => seq(
      'mod',
      field('name', $.identifier),
      field('body', $.declaration_list),
    ),

    declaration_list: $ => seq(
      '{',
      repeat($._declaration_statement),
      '}',
    ),

    _expression: $ => choice(
      $.integer_literal,
      $.identifier,
      $.parenthesized_expression,
      $.field_expression,
      $.tuple_expression,
      $.array_expression,
      $.call_expression,
      $.index_expression,
      $.unary_expression,
      $.binary_expression,
    ),

    parenthesized_expression: $ => seq(
      '(',
      $._expression,
      ')',
    ),

    field_expression: $ => seq(
      field('value', $._expression),
      field('field'), $.identifier,
    ),

    tuple_expression: $ => seq(
      '(',
      seq($._expression, ','),
      repeat(seq($._expression, ',')),
      optional($._expression),
      ')'
    ),

    array_expression: $ => seq(
      repeat(seq($._expression, ',')),
      optional($._expression),
    ),

    call_expression: $ => seq(
      field('function', $._expression),
      field('arguments', $.arguments),
    ),

    arguments: $ => seq(
      '(',
      optional(seq(
        $._expression,
        repeat(seq(',', $._expression)),
        optional(','),
      )),
      ')'
    ),

    index_expression: $ => seq(
      field('value', $_expression),
      field('arguments', $.index_arguments),
    ),

    index_arguments: $ => seq(
      '[',
      optional(seq(
        $._expression,
        repeat(seq(',', $._expression)),
        optional(','),
      )),
      ']'
    ),

    integer_literal: $ => /[0-9][0-9_]*/,

    identifier: $ => /[a-zA-Z_][a-zA-Z0-9_]+/,
  }
})