from __future__ import annotations
from dataclasses import dataclass
import itertools
import json

@dataclass
class Port:
    id_: int
    name: str
    dir: str

@dataclass
class Wire:
    id_: int
    name: str

    @property
    def dir(self):
        return None

@dataclass
class Gate:
    id_: int
    name: str
    type_: str
    pins: list[int]

    def get_comb_inputs(self) -> list[int]:
        return self.pins[1:]

    # list of (sig_id, value)
    def _eval(self, values: list[int | None]) -> list[tuple[int, int | None]]:
        if self.type_ == 'xor':
            O, A, B = self.pins
            return [(O, values[A] ^ values[B])]
        elif self.type_ == 'and':
            O, A, B = self.pins
            return [(O, values[A] & values[B])]
        elif self.type_ == 'or':
            O, A, B = self.pins
            return [(O, values[A] | values[B])]
        else:
            raise ValueError(f'Unknown gate type `{self.type_}`')


class Design:
    _name: str
    port_count: int
    wire_count: int
    _ports: list[Port]
    _wires: list[Wire]
    _gates: list[Gate]
    _signal_name_map: dict[str, Port | Wire]
    _dependency_map: dict[int, list[int]]    

    @staticmethod
    def load_from_json(dic) -> Design:
        self = Design()
        self._name = dic['name']
        port_count = len(dic['ports'])
        wire_count = len(dic['wires'])
        self._ports = []
        self._wires = []
        self._gates = []
        for i, port_dic in enumerate(dic['ports']):
            assert i == port_dic['id']
            self._ports.append(Port(
                id_=i,
                name=port_dic['name'],
                dir=port_dic['dir'],
            ))
        for i, wire_dic in enumerate(dic['wires']):
            idx = i + port_count
            assert idx == wire_dic['id']
            self._wires.append(Wire(
                id_=idx,
                name=wire_dic['name'],
            ))
        for i, gate_dic in enumerate(dic['gates']):
            assert i == gate_dic['id']
            self._gates.append(Gate(
                id_=i,
                name=gate_dic['name'],
                type_=gate_dic['type'],
                pins=gate_dic['pins']
            ))
        
        self._signal_name_map = {}
        for sig in itertools.chain(self._ports, self._wires):
            assert sig.name not in self._signal_name_map, f"signal `{sig.name}` already exists"
            self._signal_name_map[sig.name] = sig

        self._dependency_map = {}
        for gate in self._gates:
            for input in gate.get_comb_inputs():
                self._dependency_map.setdefault(input, []).append(gate.id_)

        return self

    @property
    def port_count(self):
        return len(self._ports)
    
    @property
    def wire_count(self):
        return len(self._wires)
    
    @property
    def signal_count(self):
        return self.port_count + self.wire_count
    
    @property
    def gate_count(self):
        return len(self._gates)
    
    def _get_gate_dependency_for_signal(self, sig_id: int) -> list[int]:
        return self._dependency_map.get(sig_id, [])
    
    

class DesignState:
    _design: Design
    _values: list[int | None]
    _gate_need_eval: list[bool]
    _need_eval: bool

    def __init__(self, design: Design):
        self._design = design
        self._values = [None] * design.signal_count
        self._gate_need_eval = [False] * design.gate_count
        self._need_eval = False

    def get_signal(self, name: str) -> SignalHandle:
        handle = self._design._signal_name_map[name]
        return SignalHandle(self, handle)
    
    def get_signal_list(self, name_list: list[str]) -> list[SignalHandle]:
        return [self.get_signal(name) for name in name_list]
    
    def _set_value(self, signal_id: int, value: int):
        assert isinstance(value, int)
        assert value == 0 or value == 1
        if self._values[signal_id] != value:
            self._values[signal_id] = value
            self._need_eval = True
            for gate_id in self._design._get_gate_dependency_for_signal(signal_id):
                self._gate_need_eval[gate_id] = True

    def eval(self):
        # TODO : topological sort
        for gate in self._design._gates:
            self._gate_need_eval[gate.id_] = False
            for sig, value in gate._eval(self._values):
                self._set_value(sig, value)
        assert not any(self._gate_need_eval)
        self._need_eval = False

class SignalHandle:
    _state: DesignState
    _handle: Port | Wire

    def __init__(self, state, handle):
        self._state = state
        self._handle = handle

    @property
    def name(self) -> str:
        return self._handle.name

    @property
    def value(self) -> int | None:
        assert not self._state._need_eval
        return self._state._values[self._handle.id_]
    
    @value.setter
    def value(self, value: int) -> None:
        self._state._set_value(self._handle.id_, value)

    def expect(self, expected: int):
        actual = self.value
        assert actual == expected

if __name__ == '__main__':
    with open('support/FA.json') as f:
        dic = json.load(f)
    design = Design.load_from_json(dic)
    state = DesignState(design)
    
    A, B, CI, S, CO = state.get_signal_list(['A', 'B', 'CI', 'S', 'CO'])
    
    test_cases = [
        [0, 0, 0, 0, 0],
        [0, 0, 1, 1, 0],
        [0, 1, 0, 1, 0],
        [0, 1, 1, 0, 1],
    ]
    for test_case in test_cases:
        A_, B_, CI_, S_, CO_ = test_case
        A.value = A_
        B.value = B_
        CI.value = CI_
        state.eval()
        S.expect(S_)
        CO.expect(CO_)
