use std::collections::HashMap;
use std::ops;

use super::*;
use nir::{GateKind, GraphPort, ModId, SigId};

#[derive(Clone, Debug)]
pub struct CompactSigMap<T>(Vec<Option<T>>);

impl<T> CompactSigMap<T> {
    pub fn with_sig_count(sig_count: usize) -> Self {
        let mut v = vec![];
        v.resize_with(sig_count, || None);
        CompactSigMap(v)
    }
}

impl<T> CompactSigMap<T> {
    pub fn len(&self) -> usize {
        self.0.len()
    }

    pub fn set(&mut self, index: SigId, value: T) {
        self.0[index.0 as usize] = Some(value);
    }
}

impl<T> ops::Index<nir::SigId> for CompactSigMap<T> {
    type Output = T;

    fn index(&self, index: nir::SigId) -> &Self::Output {
        self.0[index.0 as usize].as_ref().unwrap()
    }
}

#[derive(Debug)]
pub enum RuntimeKind {
    Gate(GateKind),
    User(ModId),
}

#[derive(Debug)]
pub struct CompactCell {
    kind: RuntimeKind,
    // range indexed in pin_store
    inputs: (usize, usize),
    // range indexed in pin_store
    outputs: (usize, usize),
}

#[derive(Debug)]
pub struct CompactGraph {
    sig_count: usize,
    inputs: Vec<Option<nir::SigId>>,
    outputs: Vec<nir::SigId>,
    cells: Vec<CompactCell>,
    constants: Vec<(SigId, bool)>,

    pin_store: Vec<nir::SigId>,
}

struct Mapping(HashMap<nir::SigId, nir::SigId>);

impl Mapping {
    fn from_topo(topo: &nir::TopoAnalysis) -> Self {
        let mut local_sig_counter = 0;
        let mut mapping = HashMap::new();
        for sig in topo.sig_iter() {
            mapping.insert(sig, nir::SigId(local_sig_counter));
            local_sig_counter += 1;
        }

        Mapping(mapping)
    }

    fn map_constants(&self, constants: &HashMap<SigId, bool>) -> Vec<(SigId, bool)> {
        let mapping = &self.0;
        constants.iter().map(|(k, &v)| (mapping[k], v)).collect()
    }

    fn map_ports(&self, ports: &[nir::GraphPort]) -> (Vec<Option<SigId>>, Vec<SigId>) {
        let mapping = &self.0;

        let mut inputs = vec![];
        let mut outputs = vec![];
        for port in ports {
            match port {
                GraphPort::In(input) => {
                    inputs.push(input.map(|x| mapping[&x]));
                }
                GraphPort::Out(output) => outputs.push(mapping[output]),
            }
        }

        (inputs, outputs)
    }

    // (usize, usize) is just Range<usize>
    fn map_ports_extend(
        &self,
        pin_store: &mut Vec<SigId>,
        ports: impl IntoIterator<Item = SigId>,
    ) -> (usize, usize) {
        let mapping = &self.0;

        let start = pin_store.len();
        pin_store.extend(ports.into_iter().map(|x| mapping[&x]));
        (start, pin_store.len())
    }

    fn map_gate(&self, pin_store: &mut Vec<SigId>, gate: &nir::Gate) -> CompactCell {
        let inputs = self.map_ports_extend(pin_store, gate.input_pins());
        let outputs = self.map_ports_extend(pin_store, gate.output_pins());

        CompactCell {
            kind: RuntimeKind::Gate(gate.kind),
            inputs,
            outputs,
        }
    }

    fn map_user_cell(&self, pin_store: &mut Vec<SigId>, user_cell: &nir::UserCell) -> CompactCell {
        let inputs = self.map_ports_extend(pin_store, user_cell.input_pins());
        let outputs = self.map_ports_extend(pin_store, user_cell.output_pins());

        CompactCell {
            kind: RuntimeKind::User(user_cell.mod_id),
            inputs,
            outputs,
        }
    }
}

impl CompactGraph {
    pub fn from_graph(g: &nir::Graph, topo: &nir::TopoAnalysis) -> Self {
        let sig_count = g.signal_count();
        let mapping = Mapping::from_topo(topo);

        assert!(mapping.0.len() == sig_count);

        let (inputs, outputs) = mapping.map_ports(&g.ports);
        let constants = mapping.map_constants(&g.constants);

        let mut pin_store = vec![];
        let mut cells = vec![];

        for cell_id in &topo.cell_order {
            let gate = match &g.cells[cell_id] {
                nir::Cell::Gate(gate) => mapping.map_gate(&mut pin_store, gate),
                nir::Cell::User(cell) => mapping.map_user_cell(&mut pin_store, cell),
            };
            cells.push(gate);
        }

        CompactGraph {
            sig_count,
            inputs,
            outputs,
            constants,
            cells,
            pin_store,
        }
    }

    pub fn input_count(&self) -> usize {
        self.inputs.len()
    }

    pub fn output_count(&self) -> usize {
        self.outputs.len()
    }

    pub fn used_inputs(&self) -> impl Iterator<Item = SigId> + '_ {
        self.inputs.iter().filter_map(|&x| x)
    }

    pub fn used_inputs_with_index(&self) -> impl Iterator<Item = (usize, SigId)> + '_ {
        self.inputs
            .iter()
            .enumerate()
            .filter_map(|(idx, x)| x.map(|x| (idx, x)))
    }

    pub fn _get_cell_pin(&self, pin_list: (usize, usize)) -> &[SigId] {
        &self.pin_store[pin_list.0..pin_list.1]
    }
}

pub trait EvalIn {
    fn input_count(&self) -> usize;
    fn get_input(&self, idx: usize) -> bool;
}

pub trait EvalOut {
    fn output_count(&self) -> usize;
    fn set_output(&mut self, idx: usize, value: bool);
}

impl EvalIn for &[bool] {
    fn input_count(&self) -> usize {
        self.len()
    }

    fn get_input(&self, idx: usize) -> bool {
        self[idx]
    }
}

impl EvalOut for &mut [bool] {
    fn output_count(&self) -> usize {
        self.len()
    }

    fn set_output(&mut self, idx: usize, value: bool) {
        self[idx] = value;
    }
}

pub type FnModuleMap<'a> = &'a dyn Fn(nir::ModId) -> &'a CompactGraph;

pub fn eval_graph(g: &CompactGraph, inputs: &[bool], outputs: &mut [bool], map: FnModuleMap) {
    let mut values: CompactSigMap<bool> = CompactSigMap::with_sig_count(g.sig_count);

    assert!(g.inputs.len() == inputs.input_count());
    assert!(g.outputs.len() == outputs.output_count());

    for &(const_sig, v) in &g.constants {
        values.set(const_sig, v);
    }

    for (idx, input_sig) in g.used_inputs_with_index() {
        values.set(input_sig, inputs[idx]);
    }

    for cell in &g.cells {
        let inputs = g._get_cell_pin(cell.inputs);
        let outputs = g._get_cell_pin(cell.outputs);
        match cell.kind {
            RuntimeKind::Gate(gate_kind) => {
                _eval_gate(&mut values, gate_kind, inputs, outputs);
            }
            RuntimeKind::User(mod_id) => {
                let sub_g = map(mod_id);
                let sub_inputs = Vec::from_iter(inputs.iter().map(|&sig| values[sig]));
                let mut sub_outputs = vec![false; outputs.len()];
                eval_graph(sub_g, &sub_inputs, &mut sub_outputs, map);

                for (idx, &sig) in outputs.iter().enumerate() {
                    values.set(sig, sub_outputs[idx]);
                }
            }
        }
    }

    for (idx, output_sig) in g.outputs.iter().copied().enumerate() {
        outputs[idx] = values[output_sig];
    }
}

fn _eval_gate(
    values: &mut CompactSigMap<bool>,
    gate: GateKind,
    inputs: &[SigId],
    outputs: &[SigId],
) {
    use GateKind as CK;

    debug_assert!(outputs.len() == 1);
    let v = match gate {
        CK::Const(v) => {
            debug_assert!(inputs.len() == 0);

            v
        }

        CK::Buf | CK::Not => {
            debug_assert!(inputs.len() == 1);

            values[inputs[0]]
        }

        CK::Mux => {
            debug_assert!(inputs.len() == 3);
            let v0 = values[inputs[0]];
            let v1 = values[inputs[1]];
            let v2 = values[inputs[2]];

            if v0 {
                v1
            } else {
                v2
            }
        }

        CK::And | CK::Nand => {
            let mut v = true;
            for &input in inputs {
                v &= values[input];
            }

            v
        }

        CK::Or | CK::Nor => {
            let mut v = false;
            for &input in inputs {
                v |= values[input];
            }

            v
        }

        CK::Xor | CK::Xnor => {
            let mut v = false;
            for &input in inputs {
                v ^= values[input];
            }

            v
        }
    };

    // is `gate.is_inv()`, apply the negation
    let v = v ^ gate.is_inv();

    values.set(outputs[0], v);
}
