use std::collections::HashMap;

use super::*;
use nir::const_simplify;
use nir::ModId;

#[derive(Default, Debug)]
pub struct ModInterface {
    pub ports: Vec<(String, nir::PortKind)>,
}

impl ModInterface {
    pub fn new() -> Self {
        Self::default()
    }
    pub fn add_input_port(&mut self, name: &str) {
        self.ports.push((name.into(), nir::PortKind::In));
    }
    pub fn add_output_port(&mut self, name: &str) {
        self.ports.push((name.into(), nir::PortKind::Out));
    }
    pub fn add_port(&mut self, name: &str, kind: nir::PortKind) {
        self.ports.push((name.into(), kind));
    }

    pub fn lookup_port(&self, name: &str) -> Option<(usize, nir::PortKind)> {
        for (idx, (port_name, kind)) in self.ports.iter().enumerate() {
            if name == port_name {
                return Some((idx, *kind));
            }
        }
        None
    }
}

#[derive(Default, Debug)]
pub struct Design {
    pub module_type_map: HashMap<ModId, ModInterface>,
    pub graphs: HashMap<ModId, nir::Graph>,
    pub compact_graphs: HashMap<ModId, lir::CompactGraph>,
    pub name_map: HashMap<String, ModId>,
    mod_counter: u32,
}

impl Design {
    pub fn new() -> Self {
        Self::default()
    }

    pub fn gen_mod_id(&mut self) -> ModId {
        let id = self.mod_counter;
        self.mod_counter += 1;
        ModId(id)
    }

    pub fn set_module_name(&mut self, id: ModId, name: &str) {
        use std::collections::hash_map::Entry;
        match self.name_map.entry(name.into()) {
            Entry::Occupied(e) => panic!("module `{}` already exists", e.key()),
            Entry::Vacant(e) => e.insert(id),
        };
    }
}

impl Design {
    pub fn compile(&mut self, opt_level: usize) {
        assert!(self.compact_graphs.is_empty());

        for (&id, g) in &mut self.graphs {
            let mut topo = nir::topo_analysis(g).unwrap();

            if opt_level >= 1 {
                let cp = nir::const_prop_analysis(g, &topo);
                const_simplify(g, &mut topo, cp)
            }

            let cg = lir::CompactGraph::from_graph(g, &topo);

            self.compact_graphs.insert(id, cg);
        }
    }

    pub fn eval(&self, mod_id: ModId, inputs: &[bool], outputs: &mut [bool]) {
        // fast check that the design has been compiled
        assert!(!self.compact_graphs.is_empty());

        let g = &self.compact_graphs[&mod_id];
        let map = |mod_id| &self.compact_graphs[&mod_id];

        lir::eval_graph(g, &inputs, &mut &mut *outputs, &map);
    }
}
