use std::collections::{HashMap, HashSet, VecDeque};
use std::fmt;

use crate::util::TriState;

#[derive(Clone, Copy, PartialEq, Eq, Hash)]
pub struct ModId(pub u32);

#[derive(Clone, Copy, PartialEq, Eq, Hash)]
pub struct SigId(pub u32);

#[derive(Clone, Copy, PartialEq, Eq, Hash)]
pub struct CellId(pub u32);

#[derive(Clone, Copy, PartialEq, Eq, Hash)]
pub struct RegionId(pub u32);

impl fmt::Debug for ModId {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "M_{}", self.0)
    }
}
impl fmt::Debug for SigId {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "T_{}", self.0)
    }
}

impl fmt::Debug for CellId {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "U_{}", self.0)
    }
}

impl fmt::Debug for RegionId {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "R_{}", self.0)
    }
}

#[derive(Clone, Copy, PartialEq, Eq, Debug)]
pub enum PortKind {
    In,
    Out,
}

#[derive(Clone, Copy, PartialEq, Eq, Debug)]
pub enum GateKind {
    Const(bool),

    Buf,
    Not,

    And,
    Or,
    Xor,
    Nand,
    Nor,
    Xnor,

    Mux,
}

impl GateKind {
    pub fn is_support_input_count(self, count: usize) -> bool {
        use GateKind::*;
        match self {
            Const(_) => count == 0,
            Buf | Not => count == 1,
            Mux => count == 3,
            And | Or | Xor | Nand | Nor | Xnor => true,
        }
    }
    pub fn is_binary(self) -> bool {
        use GateKind::*;
        matches!(self, And | Or | Xor | Nand | Nor | Xnor)
    }
    pub fn is_inv(self) -> bool {
        use GateKind::*;
        matches!(self, Not | Nand | Nor | Xnor)
    }
}

#[derive(Debug)]
pub struct Gate {
    pub kind: GateKind,
    pub inputs: Vec<SigId>,
    pub outputs: Vec<SigId>,
}

impl Gate {
    pub fn input_pins(&self) -> impl Iterator<Item = SigId> + '_ {
        self.inputs.iter().cloned()
    }

    pub fn output_pins(&self) -> impl Iterator<Item = SigId> + '_ {
        self.outputs.iter().cloned()
    }

    pub fn gate_name(&self) -> String {
        let input_count = self.inputs.len();
        use GateKind::*;

        let fmt_gate_2 = |name: &str| {
            if input_count == 2 {
                name.into()
            } else {
                format!("{name}_{input_count}")
            }
        };

        match self.kind {
            Const(false) => "$_lo".into(),
            Const(true) => "$_hi".into(),
            Buf => "$_buf".into(),
            Not => "$not".into(),
            Mux => "$mux".into(),
            And => fmt_gate_2("$and"),
            Or => fmt_gate_2("$or"),
            Xor => fmt_gate_2("$xor"),
            Nand => fmt_gate_2("$nand"),
            Nor => fmt_gate_2("$nor"),
            Xnor => fmt_gate_2("$xnor"),
        }
    }
}

#[derive(Debug)]
pub struct UserCell {
    pub mod_id: ModId,
    pub pins: Vec<(SigId, PortKind)>,
}

impl UserCell {
    pub fn input_pins(&self) -> impl Iterator<Item = SigId> + '_ {
        self.pins.iter().filter_map(|&(sig, kind)| match kind {
            PortKind::In => Some(sig),
            PortKind::Out => None,
        })
    }
    pub fn output_pins(&self) -> impl Iterator<Item = SigId> + '_ {
        self.pins.iter().filter_map(|&(sig, kind)| match kind {
            PortKind::Out => Some(sig),
            PortKind::In => None,
        })
    }
}

#[derive(Debug)]
pub enum Cell {
    Gate(Gate),
    User(UserCell),
}

impl Cell {
    pub fn ty_name(&self) -> String {
        match self {
            Cell::Gate(gate) => gate.gate_name(),
            Cell::User(cell) => format!("M_{}", cell.mod_id.0),
        }
    }
}

#[derive(Clone, Copy, PartialEq, Eq, Debug)]
pub enum GraphPort {
    In(Option<SigId>),
    Out(SigId),
}

// all signal must have exactly one drive:
// - input port
// - driven by cell
// - driven by `constants`
#[derive(Debug)]
pub struct Graph {
    pub signals: HashSet<SigId>,
    pub cells: HashMap<CellId, Cell>,
    pub ports: Vec<GraphPort>,
    pub constants: HashMap<SigId, bool>,

    name_map: HashMap<String, UnifyId>,

    sig_counter: u32,
    cell_counter: u32,
}

impl Graph {
    pub fn new() -> Self {
        Graph {
            signals: HashSet::new(),
            cells: HashMap::new(),
            ports: vec![],
            constants: HashMap::new(),
            name_map: HashMap::new(),
            sig_counter: 0,
            cell_counter: 0,
        }
    }

    pub fn signal_count(&self) -> usize {
        self.signals.len()
    }

    pub fn cell_count(&self) -> usize {
        self.cells.len()
    }

    pub fn _inputs(&self) -> impl Iterator<Item = SigId> + '_ {
        self.ports.iter().filter_map(|&x| match x {
            GraphPort::In(x) => x,
            GraphPort::Out(_) => None,
        })
    }

    pub fn _outputs(&self) -> impl Iterator<Item = SigId> + '_ {
        self.ports.iter().filter_map(|&x| match x {
            GraphPort::In(_) => None,
            GraphPort::Out(x) => Some(x),
        })
    }

    // internal use only, use new_(input | output | wire) instead
    fn _new_sig(&mut self) -> SigId {
        let id = self.sig_counter;
        self.sig_counter += 1;

        let sig = SigId(id);
        debug_assert!(self.signals.insert(sig));

        sig
    }

    fn _new_cell_id(&mut self) -> CellId {
        let id = self.cell_counter;
        self.cell_counter += 1;

        CellId(id)
    }

    fn _add_cell(&mut self, id: CellId, cell: Cell) {
        use std::collections::hash_map::Entry;
        match self.cells.entry(id) {
            Entry::Occupied(e) => panic!("cell {} already exists", e.key().0),
            Entry::Vacant(e) => e.insert(cell),
        };
    }

    pub fn new_gate(&mut self, gate: Gate) {
        let id = self._new_cell_id();
        self._add_cell(id, Cell::Gate(gate));
    }

    pub fn new_user_cell(&mut self, cell: UserCell) -> CellId {
        let id = self._new_cell_id();
        self._add_cell(id, Cell::User(cell));
        id
    }

    pub fn new_wire(&mut self) -> SigId {
        self._new_sig()
    }

    pub fn new_input(&mut self) -> SigId {
        let sig = self._new_sig();
        self.ports.push(GraphPort::In(Some(sig)));
        sig
    }

    pub fn new_output(&mut self) -> SigId {
        let sig = self._new_sig();
        self.ports.push(GraphPort::Out(sig));
        sig
    }
}

impl Graph {
    pub fn name_sig(&mut self, name: &str, sig: SigId) {
        use std::collections::hash_map::Entry;
        match self.name_map.entry(name.into()) {
            Entry::Occupied(e) => panic!("signal `{}` already exists", e.key()),
            Entry::Vacant(e) => e.insert(sig.into()),
        };
    }

    pub fn name_cell(&mut self, name: &str, cell: CellId) {
        use std::collections::hash_map::Entry;
        match self.name_map.entry(name.into()) {
            Entry::Occupied(e) => panic!("cell `{}` already exists", e.key()),
            Entry::Vacant(e) => e.insert(cell.into()),
        };
    }

    pub fn lookup_sig_name(&self, name: &str) -> Option<SigId> {
        match self.name_map.get(name) {
            Some(&UnifyId::Sig(sig)) => Some(sig),
            _ => None,
        }
    }

    pub fn lookup_cell_name(&self, name: &str) -> Option<CellId> {
        match self.name_map.get(name) {
            Some(&UnifyId::Cell(cell)) => Some(cell),
            _ => None,
        }
    }
}

impl Graph {
    pub fn validate_ports(&self) {
        for &sig in &self.ports {
            match sig {
                GraphPort::In(Some(sig)) => {
                    assert!(
                        self.signals.contains(&sig),
                        "input port Sig({}) is lost",
                        sig.0
                    );
                }
                GraphPort::In(None) => (),
                GraphPort::Out(sig) => {
                    assert!(
                        self.signals.contains(&sig),
                        "output port Sig({}) is lost",
                        sig.0
                    );
                }
            }
        }
    }
}

#[derive(Clone, Copy, PartialEq, Eq, Hash, Debug)]
pub enum UnifyId {
    Cell(CellId),
    Sig(SigId),
}

impl From<CellId> for UnifyId {
    fn from(v: CellId) -> Self {
        UnifyId::Cell(v)
    }
}

impl From<SigId> for UnifyId {
    fn from(v: SigId) -> Self {
        UnifyId::Sig(v)
    }
}

impl UnifyId {
    pub fn filter_cell(self) -> Option<CellId> {
        match self {
            UnifyId::Cell(cell) => Some(cell),
            _ => None,
        }
    }
    pub fn filter_sig(self) -> Option<SigId> {
        match self {
            UnifyId::Sig(sig) => Some(sig),
            _ => None,
        }
    }
}

#[derive(Default)]
struct UnifyGraphVertexData {
    incoming_degree: usize,
    out_edges: Vec<UnifyId>,
}

#[derive(Default)]
struct UnifyGraph {
    verticies: HashMap<UnifyId, UnifyGraphVertexData>,
    vertex_order: Vec<UnifyId>,
    zero_incoming_vertices: VecDeque<UnifyId>,
}

#[derive(Debug)]
pub struct TopoAnalysis {
    pub total_order: Vec<UnifyId>,
    pub cell_order: Vec<CellId>,
}

impl TopoAnalysis {
    pub fn sig_iter(&self) -> impl Iterator<Item = SigId> + '_ {
        self.total_order.iter().filter_map(|x| x.filter_sig())
    }

    pub fn cell_iter(&self) -> impl Iterator<Item = CellId> + '_ {
        self.cell_order.iter().copied()
    }

    pub fn rev_cell_iter(&self) -> impl Iterator<Item = CellId> + '_ {
        self.cell_order.iter().rev().copied()
    }
}

#[derive(Debug)]
pub enum TopoAnalysisError {
    CombLoop,
    Undriven(SigId),
    MultiDriven(SigId),
}

impl UnifyGraph {
    fn vertex_mut(&mut self, idx: UnifyId) -> &mut UnifyGraphVertexData {
        self.verticies.get_mut(&idx).unwrap()
    }
    fn add_vertex(&mut self, v: impl Into<UnifyId>) {
        self.verticies
            .insert(v.into(), UnifyGraphVertexData::default());
    }

    // src & dst must already been added to the graph
    fn add_edge(&mut self, src: impl Into<UnifyId>, dst: impl Into<UnifyId>) {
        let src = src.into();
        let dst = dst.into();
        self.vertex_mut(dst).incoming_degree += 1;
        self.vertex_mut(src).out_edges.push(dst);
    }
}

pub fn topo_analysis(g: &Graph) -> Result<TopoAnalysis, TopoAnalysisError> {
    g.validate_ports();

    let mut gg = UnifyGraph::default();
    for &sig in &g.signals {
        gg.add_vertex(sig);
    }
    for (&cell_id, cell) in &g.cells {
        gg.add_vertex(cell_id);

        match cell {
            Cell::Gate(gate) => {
                for &input in &gate.inputs {
                    gg.add_edge(input, cell_id);
                }
                for &output in &gate.outputs {
                    gg.add_edge(cell_id, output);
                }
            }

            Cell::User(cell) => {
                for &(pin, kind) in &cell.pins {
                    match kind {
                        PortKind::In => gg.add_edge(pin, cell_id),
                        PortKind::Out => gg.add_edge(cell_id, pin),
                    }
                }
            }
        }
    }

    // for all input ports & constants, add a virtual edge from super source
    for input in g._inputs() {
        let sig = UnifyId::Sig(input);
        gg.vertex_mut(sig).incoming_degree += 1;
    }
    for &con in g.constants.keys() {
        let sig = UnifyId::Sig(con);
        gg.vertex_mut(sig).incoming_degree += 1;
    }

    // check that all signal has exactly one drive
    for (&sig, data) in &gg.verticies {
        if let UnifyId::Sig(sig) = sig {
            if data.incoming_degree != 1 {
                return if data.incoming_degree == 0 {
                    Err(TopoAnalysisError::Undriven(sig))
                } else {
                    Err(TopoAnalysisError::MultiDriven(sig))
                };
            }
        }
    }

    for input in g._inputs() {
        let sig = UnifyId::Sig(input);
        gg.zero_incoming_vertices.push_back(sig);
        gg.vertex_mut(sig).incoming_degree -= 1;
    }
    for &con in g.constants.keys() {
        let sig = UnifyId::Sig(con);
        gg.zero_incoming_vertices.push_back(sig);
        gg.vertex_mut(sig).incoming_degree -= 1;
    }

    while let Some(src) = gg.zero_incoming_vertices.pop_front() {
        gg.vertex_order.push(src);

        let out_edges = std::mem::take(&mut gg.vertex_mut(src).out_edges);
        for &dst in &out_edges {
            let dst_in_degree = &mut gg.verticies.get_mut(&dst).unwrap().incoming_degree;
            *dst_in_degree -= 1;
            if *dst_in_degree == 0 {
                gg.zero_incoming_vertices.push_back(dst);
            }
        }

        gg.verticies.remove(&src);
    }

    // just a sanity check
    assert!(g.signals.len() + g.cells.len() == gg.vertex_order.len() + gg.verticies.len());

    if !gg.verticies.is_empty() {
        return Err(TopoAnalysisError::CombLoop);
    }

    let cell_order = gg
        .vertex_order
        .iter()
        .filter_map(|x| x.filter_cell())
        .collect();

    Ok(TopoAnalysis {
        total_order: gg.vertex_order,
        cell_order,
    })
}

#[derive(Debug)]
pub struct ConstPropAnalysis {
    pub value_map: HashMap<SigId, bool>,
    pub demand_cells: HashSet<CellId>,
    pub demand_signals: HashSet<SigId>,
}

fn _const_prop_cell(cell: &Gate, value_map: &mut HashMap<SigId, TriState>) {
    use GateKind as CK;

    let is_inv = cell.kind.is_inv();

    let v = match cell.kind {
        CK::Const(v) => v.into(),
        CK::Buf | CK::Not => value_map[&cell.inputs[0]],
        CK::Mux => {
            let v0 = value_map[&cell.inputs[0]];
            let v1 = value_map[&cell.inputs[1]];
            let v2 = value_map[&cell.inputs[2]];
            v0.mux(v1, v2)
        }
        CK::And | CK::Nand => {
            let mut v = TriState::Hi;

            for input in &cell.inputs {
                v = v & value_map[input];
            }

            v
        }
        CK::Or | CK::Nor => {
            let mut v = TriState::Lo;

            for input in &cell.inputs {
                v = v | value_map[input];
            }

            v
        }
        CK::Xor | CK::Xnor => {
            let mut v = TriState::Lo;

            for input in &cell.inputs {
                v = v ^ value_map[input];
            }

            v
        }
    };

    let v = if is_inv { !v } else { v };

    value_map.insert(cell.outputs[0], v);
}

pub fn const_prop_analysis(g: &Graph, topo: &TopoAnalysis) -> ConstPropAnalysis {
    let mut value_map = HashMap::new();

    for (&sig, &v) in &g.constants {
        value_map.insert(sig, v.into());
    }
    for sig in g._inputs() {
        value_map.insert(sig, TriState::X);
    }

    for cell_id in &topo.cell_order {
        let cell = &g.cells[cell_id];
        match cell {
            Cell::Gate(gate) => _const_prop_cell(gate, &mut value_map),
            Cell::User(cell) => {
                // currently treat all user cell as opaque
                // TODO: more fine-graded analysis
                for out_pin in cell.output_pins() {
                    value_map.insert(out_pin, TriState::X);
                }
            }
        }
    }

    let value_map = value_map
        .iter()
        .filter_map(|(&k, v)| v.to_option().map(|v| (k, v)))
        .collect::<HashMap<SigId, bool>>();

    let mut demand_signals = HashSet::new();
    let mut demand_cells = HashSet::new();
    demand_signals.extend(g._outputs());

    for cell_id in topo.rev_cell_iter() {
        match &g.cells[&cell_id] {
            Cell::Gate(gate) => {
                let cell_is_used = gate
                    .output_pins()
                    .any(|s| !value_map.contains_key(&s) && demand_signals.contains(&s));

                if cell_is_used {
                    demand_cells.insert(cell_id);
                    demand_signals.extend(gate.input_pins());
                }
            }

            Cell::User(cell) => {
                // TODO: more fine-graded analysis
                let cell_is_used = cell
                    .output_pins()
                    .any(|s| !value_map.contains_key(&s) && demand_signals.contains(&s));

                if cell_is_used {
                    demand_cells.insert(cell_id);
                    demand_signals.extend(cell.input_pins());
                }
            }
        }
    }

    ConstPropAnalysis {
        value_map,
        demand_signals,
        demand_cells,
    }
}

pub fn const_simplify(g: &mut Graph, topo: &mut TopoAnalysis, cp: ConstPropAnalysis) {
    let ConstPropAnalysis {
        mut value_map,
        demand_signals,
        demand_cells,
    } = cp;

    // retain only demanded signals in constants map
    value_map.retain(|sig, _| demand_signals.contains(sig));

    // retain only demanded signals in input ports
    for port in &mut g.ports {
        if let GraphPort::In(input_sig) = port {
            if let Some(sig) = *input_sig {
                if !demand_signals.contains(&sig) {
                    *input_sig = None;
                }
            }
        }
    }

    g.constants = value_map;

    g.signals = demand_signals;

    g.cells.retain(|cell_id, _| demand_cells.contains(cell_id));

    // unwrap: internal error
    *topo = topo_analysis(g).unwrap();
}
