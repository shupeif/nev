use std::ops;

use super::*;
use design::Design;
use design::ModInterface;
use nev_ast::ast;
use nir::{CellId, Gate, GateKind, Graph, ModId, PortKind, SigId};

struct GraphBuilder {
    inner: Graph,
}

impl ops::Deref for GraphBuilder {
    type Target = Graph;

    fn deref(&self) -> &Self::Target {
        &self.inner
    }
}

impl GraphBuilder {
    pub fn new() -> Self {
        Self {
            inner: Graph::new(),
        }
    }
    pub fn finish(self) -> Graph {
        self.inner
    }

    pub fn unwrap_dest(&mut self, dest: Option<SigId>) -> SigId {
        dest.unwrap_or_else(|| self.inner.new_wire())
    }
    pub fn mk_sig_const(&mut self, dest: Option<SigId>, v: bool) -> SigId {
        let dest = self.unwrap_dest(dest);
        self.inner.constants.insert(dest, v);

        dest
    }
    pub fn mk_sig_buf(&mut self, dest: Option<SigId>, src: SigId) -> SigId {
        if let Some(dest) = dest {
            self.inner.new_gate(Gate {
                kind: GateKind::Buf,
                inputs: vec![src],
                outputs: vec![dest],
            });

            dest
        } else {
            src
        }
    }
    pub fn mk_sig_uop(&mut self, dest: Option<SigId>, op: GateKind, opr1: SigId) -> SigId {
        self.mk_sig_op(dest, op, vec![opr1])
    }
    pub fn mk_sig_binop(
        &mut self,
        dest: Option<SigId>,
        op: GateKind,
        opr1: SigId,
        opr2: SigId,
    ) -> SigId {
        self.mk_sig_op(dest, op, vec![opr1, opr2])
    }
    pub fn mk_sig_op(&mut self, dest: Option<SigId>, op: GateKind, args: Vec<SigId>) -> SigId {
        assert!(op.is_support_input_count(args.len()));

        let dest = self.unwrap_dest(dest);
        self.inner.new_gate(Gate {
            kind: op,
            inputs: args,
            outputs: vec![dest],
        });

        dest
    }

    pub fn mk_user_cell(&mut self, mod_id: ModId, mod_if: &ModInterface) -> CellId {
        let pins = Vec::from_iter(
            mod_if
                .ports
                .iter()
                .map(|(_name, kind)| (self.inner.new_wire(), *kind)),
        );
        self.inner.new_user_cell(nir::UserCell { mod_id, pins })
    }
}

pub fn lower_design(de: &ast::Design) -> Design {
    let mut design = Design::new();
    let mut modules = vec![];
    for &item in de.item_list {
        match item {
            ast::PkgItem::Module(m) => {
                modules.push(m);

                let id = design.gen_mod_id();
                design.set_module_name(id, m.name.text);

                let mod_if = lower_module_interface(m);
                design.module_type_map.insert(id, mod_if);
            }
            _ => todo!(),
        }
    }

    for m in modules {
        let id = design.name_map[m.name.text];
        let m = lower_module(&design, m);
        design.graphs.insert(id, m);
    }

    design
}

pub fn lower_module_interface(m: &ast::Module) -> ModInterface {
    let mut mod_if = ModInterface::new();

    for stmt in m.stmt_list {
        match stmt {
            ast::Stmt::BitSigDecl(stmt) => {
                let kind = match stmt.kind {
                    ast::SigKind::Input => PortKind::In,
                    ast::SigKind::Output => PortKind::Out,
                    ast::SigKind::Wire => continue,
                    // ast::SigKind::Reg => unimplemented!(),
                };
                for &var in stmt.vars {
                    mod_if.add_port(var.text, kind);
                }
            }

            _ => (),
        }
    }

    mod_if
}

pub fn lower_module(de: &Design, m: &ast::Module) -> nir::Graph {
    let mut g = GraphBuilder::new();

    let mut bit_sig_decl = vec![];
    let mut cell_decl = vec![];
    let mut assigns = vec![];
    for stmt in m.stmt_list {
        match stmt {
            ast::Stmt::Assign(stmt) => assigns.push(stmt),
            ast::Stmt::BitSigDecl(stmt) => bit_sig_decl.push(stmt),
            ast::Stmt::CellDecl(stmt) => cell_decl.push(stmt),
            ast::Stmt::SigDecl(..) => unimplemented!(),
        }
    }

    for &sig_decl in &bit_sig_decl {
        for &var in sig_decl.vars {
            let sig = match sig_decl.kind {
                ast::SigKind::Input => g.inner.new_input(),
                ast::SigKind::Output => g.inner.new_output(),
                ast::SigKind::Wire => g.inner.new_wire(),
                // ast::SigKind::Reg => unimplemented!(),
            };
            g.inner.name_sig(var.text, sig);
        }
    }

    for &cell_decl in &cell_decl {
        let mod_id = de.name_map[cell_decl.ty.text];
        let mod_if = &de.module_type_map[&mod_id];

        for &var in cell_decl.vars {
            let id = g.mk_user_cell(mod_id, mod_if);
            g.inner.name_cell(var.text, id);
        }
    }

    for stmt in &assigns {
        let lhs = resolve_pin(&g, de, &stmt.lhs);
        let _lhs = lower_expr(&mut g, de, Some(lhs), &stmt.rhs);
        assert!(lhs == _lhs);
    }

    g.finish()
}

// invariant: if `dest` is `Some(var)`, the return value must be `var`
// otherwise, it's a newly generated wire
fn lower_expr(g: &mut GraphBuilder, de: &Design, dest: Option<SigId>, e: &ast::Expr) -> SigId {
    use ast::Binop;
    use ast::Expr;
    use ast::Uop;
    use nir::GateKind as CK;
    match e {
        &Expr::Paren(e) => lower_expr(g, de, dest, e),
        &Expr::BitLit(v) => g.mk_sig_const(dest, v),
        &Expr::IntLit(_) => unimplemented!("lower: expr intlit"),
        &Expr::StringLit(_) => unimplemented!("lower: expr stringlit"),
        &Expr::Var(_) | Expr::Dot(_, _) => {
            let src = resolve_pin(g, de, e);
            g.mk_sig_buf(dest, src)
        }
        &Expr::Uop(op, opr1) => {
            let op = match op {
                Uop::Not => CK::Not,
                Uop::Neg => unimplemented!(),
            };

            let opr1 = lower_expr(g, de, None, opr1);
            g.mk_sig_uop(dest, op, opr1)
        }
        &Expr::Unpack(_) => unimplemented!("lower: expr unpack"),
        &Expr::Tuple(_) => unimplemented!("lower: expr tuple"),
        &Expr::List(_) => unimplemented!("lower: expr list"),
        &Expr::Binop(op, opr1, opr2) => {
            let op = match op {
                Binop::And => CK::And,
                Binop::Or => CK::Or,
                Binop::Xor => CK::Xor,
                Binop::Nand => CK::Nand,
                Binop::Nor => CK::Nor,
                Binop::Xnor => CK::Xnor,
            };

            let opr1 = lower_expr(g, de, None, opr1);
            let opr2 = lower_expr(g, de, None, opr2);
            g.mk_sig_binop(dest, op, opr1, opr2)
        }
        &Expr::Call(func, args) => {
            fn is_builtin_func(func: &str) -> Option<CK> {
                Some(match func {
                    "$buf" => CK::Buf,
                    "$not" => CK::Not,
                    "$mux" => CK::Mux,
                    "$and" => CK::And,
                    "$or" => CK::Or,
                    "$xor" => CK::Xor,
                    "$nand" => CK::Nand,
                    "$nor" => CK::Nor,
                    "$xnor" => CK::Xnor,
                    _ => return None,
                })
            }

            let op = is_builtin_func(func.text)
                .unwrap_or_else(|| panic!("compile error: unknown function `{}`", func.text));

            assert!(
                op.is_support_input_count(args.len()),
                "compile error: `{}` can not be called with {} arguments",
                func.text,
                args.len(),
            );

            let args = args.iter().map(|e| lower_expr(g, de, None, e)).collect();
            g.mk_sig_op(dest, op, args)
        }
        &Expr::Index(..) => unimplemented!(),
        &Expr::ApplyGeneric(..) => unimplemented!(),
        &Expr::TypeTuple(..) => unimplemented!(),
    }
}

fn resolve_pin(g: &Graph, de: &Design, pin: &ast::Expr) -> SigId {
    use ast::Expr;
    match pin {
        &Expr::Var(v) => g
            .lookup_sig_name(v.text)
            .unwrap_or_else(|| panic!("compile error: signal `{}` not found", v.text)),

        &Expr::Dot(cell_name, pin) => {
            let cell_name = match cell_name {
                &Expr::Var(name) => name.text,
                _ => panic!("compile error: only simple dot expression is supported"),
            };

            let cell = g
                .lookup_cell_name(cell_name)
                .unwrap_or_else(|| panic!("compile error: cell `{cell_name}` not found"));

            let cell = match &g.cells[&cell] {
                nir::Cell::Gate(_) => unreachable!(),
                nir::Cell::User(cell) => cell,
            };

            let (port_idx, port_kind) = de.module_type_map[&cell.mod_id]
                .lookup_port(pin.text)
                .unwrap_or_else(|| {
                    panic!(
                        "compile error: cell `{cell_name}` has no pin `{pin}`",
                        pin = pin.text
                    )
                });

            assert!(port_kind == cell.pins[port_idx].1);

            cell.pins[port_idx].0
        }

        _ => unreachable!("ICE"),
    }
}
