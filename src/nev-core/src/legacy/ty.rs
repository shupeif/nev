use std::fmt::{Display, Formatter};

#[derive(Copy, Clone, PartialEq, Eq, Hash, Debug)]
pub struct Ty(u32);

#[derive(Copy, Clone, PartialEq, Eq, Hash, Debug)]
pub struct TyCtor(u32);

#[derive(Clone, PartialEq, Eq, Hash, Debug)]
pub enum TyKind {
    Leaf(TyCtor),
    Apply(TyCtor, Vec<GenericValue>),
}

#[derive(Clone, Copy, PartialEq, Eq, Hash, Debug)]
pub enum GenericValue {
    Type(Ty),
    Int(i32),
}

#[derive(Copy, Clone, PartialEq, Eq, Hash, Debug)]
pub enum GenericParam {
    Type,
    Int,
}

pub enum GenericKind {
    Leaf,
    Ctor(Vec<GenericParam>),
}

impl Ty {
    pub fn get(self, it: &Interner) -> &TyKind {
        it.get_type(self)
    }
    pub fn display(self, it: &Interner) -> TyDisplay {
        TyDisplay { it, ty: self }
    }
}

impl TyCtor {
    pub fn display(self, it: &Interner) -> &str {
        it.get_type_constructor_name(self)
    }
}

pub struct TyDisplay<'a> {
    it: &'a Interner,
    ty: Ty,
}

fn display_ty(f: &mut Formatter, ty: Ty, it: &Interner) -> std::fmt::Result {
    match ty.get(it) {
        TyKind::Leaf(ty) => f.write_str(ty.display(it)),
        TyKind::Apply(ctor, ref args) => {
            f.write_str(ctor.display(it))?;
            f.write_str("[")?;
            if let &[first, ref rest @ ..] = &args[..] {
                display_generic_value(f, first, it)?;
                for &x in rest {
                    f.write_str(", ")?;
                    display_generic_value(f, x, it)?;
                }
            }
            f.write_str("]")
        }
    }
}

fn display_generic_value(f: &mut Formatter, v: GenericValue, it: &Interner) -> std::fmt::Result {
    match v {
        GenericValue::Type(ty) => display_ty(f, ty, it),
        GenericValue::Int(v) => Display::fmt(&v, f),
    }
}

impl<'a> Display for TyDisplay<'a> {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        display_ty(f, self.ty, self.it)
    }
}

pub struct Interner {}

impl Interner {
    fn get_type(&self, _ty: Ty) -> &TyKind {
        todo!()
    }

    fn get_type_constructor_name(&self, _ty: TyCtor) -> &str {
        todo!()
    }
}
