pub mod diag;
pub mod hir;
pub mod legacy;
pub mod lower;
pub mod util;

pub use legacy::design::Design;
pub use legacy::lir::CompactGraph;
pub use legacy::nir::Graph;
pub use legacy::nir::ModId;

pub use legacy::lower::lower_design;
