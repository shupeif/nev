use crate::diag::{self, DiagResult};
use crate::hir::ty::{FuncTy, ModTy, PortDecl, PortKind, Ty};
use crate::hir::{self, HExpr, HGraph, HLit};
use nev_ast::ast;

use nev_util::nev_error;

pub fn lower_design(nd: &ast::Design) -> DiagResult<hir::Design> {
    let mut design = hir::Design::default();

    let mut module_list = vec![];
    let mut function_list = vec![];

    for &item in nd.item_list {
        match item {
            ast::PkgItem::Module(m) => {
                let mid = design.new_mod_id();
                let mod_ty = lower_mod_ty(m)?;
                design
                    .set_module_type(mid, m.name.text.into(), mod_ty)
                    .map_err(|_e| diag::name_redefined_module(m.name))?;

                module_list.push((mid, m));
            }
            ast::PkgItem::Function(f) => {
                let fid = design.new_func_id();
                let fun_ty = lower_fun_ty(f)?;
                design
                    .set_function_type(fid, f.name.text, fun_ty)
                    .map_err(|_e| diag::name_redefined_function(f.name))?;

                function_list.push((fid, f));
            }
        }
    }

    for &(mid, m) in &module_list {
        let body = lower_module_body(&design, m)?;
        design.set_module_body(mid, body);
    }

    for &(_fid, f) in &function_list {
        log::warn!(
            "function support is incomplete. Ignore function `{}`",
            f.name.text
        );
    }

    Ok(design)
}

pub fn lower_fun_ty(nd: &ast::Function) -> DiagResult<FuncTy> {
    let mut params = vec![];
    for &param in nd.param_list {
        let param_ty = param
            .ty
            .unwrap_or_else(|| nev_error!("type is required for parameter"));
        let param_ty = lower_ty(param_ty)?;
        params.push(param_ty);
    }
    let ret = lower_ty(nd.ret_ty)?;
    Ok(FuncTy::new(params, ret))
}

struct PortBuilder {
    ports: Vec<PortDecl>,
}

impl PortBuilder {
    fn new() -> Self {
        PortBuilder { ports: vec![] }
    }
    fn add_port(&mut self, name: ast::Ident, ty: Ty, kind: PortKind) -> DiagResult<()> {
        use itertools::any;
        if any(&self.ports, |p| p.name == name.text) {
            return Err(diag::name_redefined_port(name).into());
        }
        self.ports.push(PortDecl {
            name: name.text.into(),
            ty,
            kind,
        });
        Ok(())
    }
    fn build(self) -> ModTy {
        ModTy::new(self.ports)
    }
}

pub fn lower_mod_ty(nd: &ast::Module) -> DiagResult<ModTy> {
    use ast::{SigKind, Stmt};

    let mut ports = PortBuilder::new();
    for &stmt in nd.stmt_list {
        match stmt {
            Stmt::Assign(_) | Stmt::CellDecl(_) => {}
            Stmt::SigDecl(stmt) => {
                let kind = match stmt.kind {
                    SigKind::Wire => continue,

                    SigKind::Input => PortKind::In,
                    SigKind::Output => PortKind::Out,
                };

                let name = stmt.name;
                let ty = stmt
                    .ty
                    .unwrap_or_else(|| nev_error!("type is required for port"));
                let ty = lower_ty(ty)?;

                ports.add_port(name, ty, kind)?;
            }

            Stmt::BitSigDecl(stmt) => {
                let kind = match stmt.kind {
                    SigKind::Wire => continue,

                    SigKind::Input => PortKind::In,
                    SigKind::Output => PortKind::Out,
                    // SigKind::Reg => unimplemented!(),
                };

                for &var in stmt.vars {
                    ports.add_port(var, Ty::mk_bit(), kind)?;
                }
            }
        }
    }

    Ok(ports.build())
}

fn lower_sigkind(nd: ast::SigKind) -> hir::VarKind {
    match nd {
        ast::SigKind::Input => hir::VarKind::In,
        ast::SigKind::Output => hir::VarKind::Out,
        ast::SigKind::Wire => hir::VarKind::Local,
        // ast::SigKind::Reg => unimplemented!(),
    }
}

pub fn lower_module_body(de: &hir::Design, nd: &ast::Module) -> DiagResult<hir::HGraph> {
    use ast::Stmt;
    let mut g_ = hir::HGraph::new();
    let g = &mut g_;

    for &stmt in nd.stmt_list {
        match stmt {
            Stmt::Assign(_) => {}

            Stmt::CellDecl(stmt) => {
                let mid = de.resolve_module(stmt.ty)?;

                for &var in stmt.vars {
                    g.add_cell(var.text, mid)
                        .map_err(|_e| diag::name_redefined_var(var))?;
                }
            }

            Stmt::BitSigDecl(stmt) => {
                let kind = lower_sigkind(stmt.kind);
                for &var in stmt.vars {
                    g.add_var(var.text, kind, Ty::mk_bit())
                        .map_err(|_e| diag::name_redefined_var(var))?;
                }
            }
            Stmt::SigDecl(..) => unimplemented!(),
        }
    }

    for &stmt in nd.stmt_list {
        match stmt {
            Stmt::CellDecl(_) | Stmt::BitSigDecl(_) => {}
            Stmt::SigDecl(stmt) => {
                if let Some(body) = stmt.body {
                    log::warn!("var initializer support is incomplete");
                    let lhs = g.lookup_var(stmt.name.text).unwrap();
                    let lhs = g.add_expr(HExpr::Var(lhs));
                    let rhs = lower_expr(de, g, body)?;
                    g.add_conn(lhs, rhs);
                }
            }

            Stmt::Assign(stmt) => {
                let lhs = lower_expr(de, g, stmt.lhs)?;
                let rhs = lower_expr(de, g, stmt.rhs)?;
                g.add_conn(lhs, rhs);
            }
        }
    }

    Ok(g_)
}

pub enum TyEval {
    Ty(Ty),
    Int(usize),

    // type constructor of `bits`
    Bits,
}

fn lower_ty(nd: ast::PExpr) -> DiagResult<Ty> {
    match lower_ty_(nd) {
        Ok(TyEval::Ty(ty)) => Ok(ty),

        Ok(_) => Err(diag::IllformedType {}.into()),
        Err(e) => Err(e),
    }
}

fn lower_ty_(nd: ast::PExpr) -> DiagResult<TyEval> {
    use ast::Expr;
    match nd {
        &Expr::Var(name) => match name.text {
            "bit" => Ok(TyEval::Ty(Ty::mk_bit())),
            "int" => Ok(TyEval::Ty(Ty::mk_int())),
            "bits" => Ok(TyEval::Bits),

            _ => Err(diag::IllformedType {}.into()),
        },
        &Expr::IntLit(l) => {
            let l = l.raw_text.parse::<usize>().unwrap();
            Ok(TyEval::Int(l))
        }
        &Expr::ApplyGeneric(obj, args) => {
            todo!()
        }
        &Expr::TypeTuple(type_list) => {
            let mut args = vec![];
            for ty in type_list {
                args.push(lower_ty(ty)?);
            }
            Ok(TyEval::Ty(Ty::mk_tuple(args)))
        }

        _ => Err(diag::IllformedType {}.into()),
    }
}

pub fn lower_expr(de: &hir::Design, g: &mut HGraph, nd: ast::PExpr) -> DiagResult<hir::ExprId> {
    fn lower_expr_list(
        de: &hir::Design,
        g: &mut HGraph,
        list: &[ast::PExpr],
    ) -> DiagResult<Vec<hir::ExprId>> {
        let mut lowered_list = vec![];
        for &nd in list {
            lowered_list.push(lower_expr(de, g, nd)?);
        }
        Ok(lowered_list)
    }

    use ast::Expr;
    match nd {
        &Expr::Paren(e) => lower_expr(de, g, e),
        &Expr::BitLit(l) => Ok(g.add_expr(HExpr::Lit(HLit::Bit(l)))),
        &Expr::IntLit(l) => {
            let v = l.raw_text.parse().unwrap();
            Ok(g.add_expr(HExpr::Lit(HLit::Int(v))))
        }
        &Expr::StringLit(_l) => {
            todo!()
        }
        &Expr::Var(name) => {
            let var = g
                .lookup_var(name.text)
                .ok_or_else(|| diag::name_not_found_var(name))?;
            Ok(g.add_expr(HExpr::Var(var)))
        }
        &Expr::Dot(name, field) => {
            let name = match name {
                &Expr::Var(name) => name,

                _ => todo!(),
            };

            let (cid, mid) = g
                .lookup_cell(name.text)
                .ok_or_else(|| diag::name_not_found_cell(name))?;

            let (pid, _port_kind) = de
                .get_module_type(mid)
                .find_port(field.text)
                .ok_or_else(|| diag::name_redefined_port(field))?;

            Ok(g.add_expr(HExpr::Pin(cid, pid)))
        }
        &Expr::Uop(uop, opr) => {
            let uop = match uop {
                ast::Uop::Not => hir::HUop::Not,
                ast::Uop::Neg => hir::HUop::Neg,
            };
            let opr = lower_expr(de, g, opr)?;
            Ok(g.add_expr(HExpr::Uop(uop, opr)))
        }
        &Expr::Binop(bop, opr1, opr2) => {
            use {ast::Binop, hir::HBinop};
            let bop = match bop {
                Binop::And => HBinop::And,
                Binop::Or => HBinop::Or,
                Binop::Xor => HBinop::Xor,
                Binop::Nand => HBinop::Nand,
                Binop::Nor => HBinop::Nor,
                Binop::Xnor => HBinop::Xnor,
            };

            let opr1 = lower_expr(de, g, opr1)?;
            let opr2 = lower_expr(de, g, opr2)?;

            Ok(g.add_expr(HExpr::Binop(bop, opr1, opr2)))
        }
        &Expr::Tuple(opr_list) => {
            let opr_list = lower_expr_list(de, g, opr_list)?;
            Ok(g.add_expr(HExpr::Tuple(opr_list)))
        }
        &Expr::List(opr_list) => {
            let opr_list = lower_expr_list(de, g, opr_list)?;
            Ok(g.add_expr(HExpr::List(opr_list)))
        }
        &Expr::Unpack(opr) => {
            let opr = lower_expr(de, g, opr)?;
            Ok(g.add_expr(HExpr::Unpack(opr)))
        }
        &Expr::Index(obj, args) => {
            let obj = lower_expr(de, g, obj)?;
            let args = lower_expr_list(de, g, args)?;
            Ok(g.add_expr(HExpr::Index(obj, args)))
        }

        &Expr::Call(func, args) => {
            let fid = de.resolve_function(func)?;
            let args = lower_expr_list(de, g, args)?;
            Ok(g.add_expr(HExpr::Call(fid, args)))
        }

        &Expr::ApplyGeneric(..) => unimplemented!(),
        &Expr::TypeTuple(..) => panic!("CE"),
    }
}

impl hir::Design {
    fn resolve_module(&self, m: ast::Ident) -> DiagResult<hir::ModId> {
        Ok(self
            .resolve(m.text)
            .ok_or_else(|| diag::name_not_found_function(m))?
            .try_as_module()
            .ok_or_else(|| diag::name_expect_function(m))?)
    }
    fn resolve_function(&self, func: ast::Ident) -> DiagResult<hir::FuncId> {
        Ok(self
            .resolve(func.text)
            .ok_or_else(|| diag::name_not_found_function(func))?
            .try_as_function()
            .ok_or_else(|| diag::name_expect_function(func))?)
    }
}
