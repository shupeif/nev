use std::cell::Cell;
use std::fmt;
use std::ops;

#[derive(Clone, Copy, PartialEq, Eq, Hash)]
pub enum TriState {
    Lo,
    Hi,
    X,
}

impl fmt::Debug for TriState {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let s = match self {
            Self::Hi => "1",
            Self::Lo => "0",
            Self::X => "x",
        };
        f.pad(s)
    }
}

impl fmt::Display for TriState {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        fmt::Debug::fmt(self, f)
    }
}

impl ops::Not for TriState {
    type Output = Self;

    fn not(self) -> Self::Output {
        use TriState::*;
        match self {
            Hi => Lo,
            Lo => Hi,
            X => X,
        }
    }
}

impl ops::BitAnd for TriState {
    type Output = Self;

    fn bitand(self, rhs: Self) -> Self::Output {
        use TriState::*;
        match (self, rhs) {
            (Hi, Hi) => Hi,
            (Lo, _) => Lo,
            (_, Lo) => Lo,
            _ => X,
        }
    }
}

impl ops::BitOr for TriState {
    type Output = Self;

    fn bitor(self, rhs: Self) -> Self::Output {
        use TriState::*;
        match (self, rhs) {
            (Hi, _) => Hi,
            (_, Hi) => Hi,
            (Lo, Lo) => Lo,
            _ => X,
        }
    }
}

impl ops::BitXor for TriState {
    type Output = Self;

    fn bitxor(self, rhs: Self) -> Self::Output {
        use TriState::*;
        match (self, rhs) {
            (Lo, Lo) => Lo,
            (Lo, Hi) => Hi,
            (Hi, Lo) => Hi,
            (Hi, Hi) => Lo,
            _ => X,
        }
    }
}

impl TriState {
    pub fn mux(self, br1: Self, br2: Self) -> Self {
        use TriState::*;
        match self {
            Hi => br1,
            Lo => br2,
            _ => match (br1, br2) {
                (Hi, Hi) => Hi,
                (Lo, Lo) => Lo,
                _ => X,
            },
        }
    }
}

impl From<bool> for TriState {
    fn from(v: bool) -> Self {
        match v {
            true => Self::Hi,
            false => Self::Lo,
        }
    }
}

impl TriState {
    pub fn to_option(self) -> Option<bool> {
        use TriState::*;
        match self {
            Hi => Some(true),
            Lo => Some(false),
            X => None,
        }
    }
}

#[derive(Default)]
pub struct IdCounter(Cell<u32>);

impl fmt::Debug for IdCounter {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        // force compact print style even in pretty printing
        write!(f, "IdCounter({})", self.0.get())
    }
}

impl IdCounter {
    pub fn new() -> Self {
        Self::default()
    }

    pub fn new_id(&self) -> u32 {
        let id = self.0.get() + 1;
        self.0.set(id);
        id
    }
}

#[derive(Debug)]
pub struct OccupiedError;
pub trait TryInsertExt<K, V> {
    fn try_insert_(&mut self, key: K, value: V) -> Result<(), OccupiedError>;
    fn insert_unwrap_(&mut self, key: K, value: V);
}

impl<K: Eq + std::hash::Hash, V> TryInsertExt<K, V> for std::collections::HashMap<K, V> {
    fn try_insert_(&mut self, key: K, value: V) -> Result<(), OccupiedError> {
        use std::collections::hash_map::Entry;
        match self.entry(key) {
            Entry::Vacant(e) => {
                e.insert(value);
                Ok(())
            }
            Entry::Occupied(_e) => Err(OccupiedError),
        }
    }

    fn insert_unwrap_(&mut self, key: K, value: V) {
        use std::collections::hash_map::Entry;
        match self.entry(key) {
            Entry::Vacant(e) => {
                e.insert(value);
            }

            Entry::Occupied(_e) => unreachable!(),
        }
    }
}
