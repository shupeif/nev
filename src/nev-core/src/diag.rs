use nev_ast::ast;

pub type DiagResult<T> = Result<T, Box<DiagImpl>>;
type Span = (usize, usize);

#[derive(Debug)]
pub struct DiagImpl {
    pub slug: &'static str,
    pub code: usize,
    pub span: Option<(usize, usize)>,
    pub display: String,
}

impl<T: Diagnostic> From<T> for Box<DiagImpl> {
    fn from(value: T) -> Self {
        value.into_diag()
    }
}

impl std::fmt::Display for DiagImpl {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "Compile Error: {} (E{:04} {})",
            self.display, self.code, self.slug
        )
    }
}

impl std::error::Error for Box<DiagImpl> {}

pub trait Diagnostic: std::fmt::Debug + Sized + 'static {
    const SLUG: &'static str;
    const CODE: usize = 0;

    fn display(&self) -> String;
    fn span(&self) -> Option<(usize, usize)>;

    fn into_diag(self) -> Box<DiagImpl> {
        if std::env::var("NEV_PANIC").as_deref() == Ok("1") {
            panic!("intentional panic (NEV_PANIC=1): {self:?}");
        }

        Box::new(DiagImpl {
            slug: Self::SLUG,
            code: Self::CODE,
            span: self.span(),
            display: self.display(),
        })
    }

    fn into_diag_err<T>(self) -> DiagResult<T> {
        Err(self.into_diag())
    }
}

#[derive(Debug)]
pub struct GenericError {
    text: &'static str,
}

impl Diagnostic for GenericError {
    const SLUG: &'static str = "generic-error";

    fn display(&self) -> String {
        self.text.into()
    }
    fn span(&self) -> Option<Span> {
        None
    }
}

#[derive(Debug)]
pub struct NameNotFound {
    kind: &'static str, // "module"
    name: String,
    span: Span,
}

impl Diagnostic for NameNotFound {
    const SLUG: &'static str = "name-not-found";

    fn display(&self) -> String {
        format!("missing definition of {} `{}`", self.kind, self.name)
    }
    fn span(&self) -> Option<Span> {
        Some(self.span)
    }
}

#[derive(Debug)]
pub struct NameExpectKind {
    kind: &'static str, // "module"
    name: String,
    span: Span,
}

impl Diagnostic for NameExpectKind {
    const SLUG: &'static str = "name-not-found";

    fn display(&self) -> String {
        format!(
            "`{name}` is expected to {kind}, but not",
            kind = self.kind,
            name = self.name
        )
    }
    fn span(&self) -> Option<Span> {
        Some(self.span)
    }
}

#[derive(Debug)]
pub struct NameRedefined {
    kind: &'static str, // "module"
    name: String,
    span: Span,
}

impl Diagnostic for NameRedefined {
    const SLUG: &'static str = "name-redefined";

    fn display(&self) -> String {
        format!("duplicate definition of {} `{}`", self.kind, self.name)
    }
    fn span(&self) -> Option<Span> {
        Some(self.span)
    }
}

#[derive(Debug)]
pub struct IllformedType {}

impl Diagnostic for IllformedType {
    const SLUG: &'static str = "illformed-type";

    fn display(&self) -> String {
        "ill-formed type expression".into()
    }
    fn span(&self) -> Option<Span> {
        None
    }
}

pub fn name_not_found_module(name: ast::Ident) -> NameNotFound {
    NameNotFound {
        kind: "module",
        name: name.text.into(),
        span: name.span,
    }
}

pub fn name_not_found_function(name: ast::Ident) -> NameNotFound {
    NameNotFound {
        kind: "function",
        name: name.text.into(),
        span: name.span,
    }
}

pub fn name_not_found_var(name: ast::Ident) -> NameNotFound {
    NameNotFound {
        kind: "var",
        name: name.text.into(),
        span: name.span,
    }
}

pub fn name_not_found_cell(name: ast::Ident) -> NameNotFound {
    NameNotFound {
        kind: "cell",
        name: name.text.into(),
        span: name.span,
    }
}

pub fn name_redefined_port(name: ast::Ident) -> NameRedefined {
    NameRedefined {
        kind: "port",
        name: name.text.into(),
        span: name.span,
    }
}
pub fn name_redefined_module(name: ast::Ident) -> NameRedefined {
    NameRedefined {
        kind: "module",
        name: name.text.into(),
        span: name.span,
    }
}
pub fn name_redefined_function(name: ast::Ident) -> NameRedefined {
    NameRedefined {
        kind: "function",
        name: name.text.into(),
        span: name.span,
    }
}
pub fn name_redefined_var(name: ast::Ident) -> NameRedefined {
    NameRedefined {
        kind: "var",
        name: name.text.into(),
        span: name.span,
    }
}
pub fn name_expect_module(name: ast::Ident) -> NameExpectKind {
    NameExpectKind {
        kind: "module",
        name: name.text.into(),
        span: name.span,
    }
}
pub fn name_expect_function(name: ast::Ident) -> NameExpectKind {
    NameExpectKind {
        kind: "function",
        name: name.text.into(),
        span: name.span,
    }
}
pub fn invalid_literal() -> GenericError {
    GenericError {
        text: "the literal is invalid",
    }
}
