#[derive(Clone)]
pub struct Ty {
    kind: TyKind,
}

impl Ty {
    pub fn resolve(&self) -> &TyKind {
        &self.kind
    }
    pub fn mk_int() -> Self {
        Ty { kind: TyKind::Int }
    }
    pub fn mk_bit() -> Self {
        Ty { kind: TyKind::Bit }
    }
    pub fn mk_bits(width: usize) -> Self {
        Ty {
            kind: TyKind::Bits(width),
        }
    }
    pub fn mk_tuple(args: Vec<Ty>) -> Self {
        Ty {
            kind: TyKind::Tuple(args),
        }
    }
    pub fn mk_unpack_tuple(args: Vec<Ty>) -> Self {
        Ty {
            kind: TyKind::UnpackTuple(args),
        }
    }
    pub fn mk_unpack_tuple_repeat(sub: Ty, repeat: usize) -> Self {
        Ty {
            kind: TyKind::UnpackTuple(vec![sub; repeat]),
        }
    }

    pub fn is_bit(&self) -> bool {
        matches!(self.kind, TyKind::Bit)
    }
    pub fn is_int(&self) -> bool {
        matches!(self.kind, TyKind::Int)
    }
}

impl PartialEq for Ty {
    fn eq(&self, other: &Self) -> bool {
        self.kind == other.kind
    }
}

impl Eq for Ty {}

impl std::fmt::Debug for Ty {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        self.kind.fmt(f)
    }
}

#[derive(Clone, PartialEq, Eq, Debug)]
pub enum TyKind {
    Int,
    Bit,
    Bits(usize),
    Tuple(Vec<Ty>),
    UnpackTuple(Vec<Ty>),
}

#[derive(Clone, PartialEq, Eq, Debug)]
pub struct FuncTy {
    pub params: Vec<Ty>,
    pub ret: Ty,
}

impl FuncTy {
    pub fn new(params: Vec<Ty>, ret: Ty) -> Self {
        FuncTy { params, ret }
    }
    pub fn param_count(&self) -> usize {
        self.params.len()
    }
    pub fn param_ty(&self, index: usize) -> &Ty {
        &self.params[index]
    }
}

#[derive(Clone, Copy, PartialEq, Eq, Debug)]
pub enum PortKind {
    In,
    Out,
}

#[derive(Debug)]
pub struct PortDecl {
    pub name: String,
    pub ty: Ty,
    pub kind: PortKind,
}

#[derive(Debug)]
pub struct ModTy {
    ports: Vec<PortDecl>,
}

pub struct PortRedefined {
    pub ports: Vec<PortDecl>,
    pub indices: (usize, usize),
}

impl ModTy {
    pub fn new(ports: Vec<PortDecl>) -> Self {
        use itertools::{all, enumerate};
        // check port name duplication
        #[rustfmt::skip]
        assert!(
            all(
                enumerate(&ports),
                |(i, p)| all(&ports[..i], |pp| p.name != pp.name),
            ),
            "ICE: {:?}", ports,
        );

        ModTy { ports }
    }
    pub fn get_port(&self, pid: usize) -> &PortDecl {
        &self.ports[pid]
    }
    pub fn find_port(&self, name: &str) -> Option<(usize, &PortDecl)> {
        for (idx, port) in self.ports.iter().enumerate() {
            if name == port.name {
                return Some((idx, port));
            }
        }

        None
    }
}
