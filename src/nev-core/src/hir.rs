pub mod ty;

use std::cell::Ref;
use std::cell::RefCell;
use std::cell::RefMut;
use std::collections::HashMap;

use crate::util::IdCounter;
use crate::util::TryInsertExt;

use self::ty::Ty;
use self::ty::TyKind;

#[derive(Clone, Copy, PartialEq, Eq, Hash)]
pub struct ModId(pub u32);

#[derive(Clone, Copy, PartialEq, Eq, Hash)]
pub struct FuncId(pub u32);

#[derive(Clone, Copy, PartialEq, Eq, Hash)]
pub struct VarId(pub u32);

#[derive(Clone, Copy, PartialEq, Eq, Hash)]
pub struct ExprId(pub u32);

#[derive(Clone, Copy, PartialEq, Eq, Hash)]
pub struct CellId(pub u32);

#[derive(Clone, Copy, PartialEq, Eq, Hash)]
pub enum DefId {
    Var(VarId),
    Cell(CellId),
}

impl std::fmt::Debug for ModId {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "M#{}", self.0)
    }
}

impl std::fmt::Debug for FuncId {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "F#{}", self.0)
    }
}

impl std::fmt::Debug for VarId {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "V#{}", self.0)
    }
}

impl std::fmt::Debug for ExprId {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "E#{}", self.0)
    }
}

impl std::fmt::Debug for CellId {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "C#{}", self.0)
    }
}

impl std::fmt::Debug for DefId {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::Var(arg0) => arg0.fmt(f),
            Self::Cell(arg0) => arg0.fmt(f),
        }
    }
}

#[derive(Debug)]
pub struct HCell {
    pub mid: ModId,
}

#[derive(Debug)]
pub struct HVar {
    pub ty: Ty,
    pub kind: VarKind,
}

#[derive(Clone, Copy, PartialEq, Eq, Debug)]
pub enum HUop {
    Not,
    Neg,
}

#[derive(Clone, Copy, PartialEq, Eq, Debug)]
pub enum HBinop {
    And,
    Or,
    Xor,
    Nand,
    Nor,
    Xnor,
}

#[derive(Debug)]
pub enum HExpr {
    Lit(HLit),
    Var(VarId),
    Pin(CellId, usize),
    Unpack(ExprId),
    Tuple(Vec<ExprId>),
    List(Vec<ExprId>),
    Uop(HUop, ExprId),
    Binop(HBinop, ExprId, ExprId),
    Index(ExprId, Vec<ExprId>),
    Call(FuncId, Vec<ExprId>),
}

#[derive(Debug)]
pub enum HLit {
    Bit(bool),
    Int(u32),
}

impl HLit {
    pub fn get_ty(&self) -> Ty {
        match self {
            HLit::Bit(_) => Ty::mk_bit(),
            HLit::Int(_) => Ty::mk_int(),
        }
    }
}

#[derive(Default, Debug)]
pub struct HFunc {
    pub vars: HashMap<VarId, HVar>,
    pub exprs: HashMap<ExprId, HExpr>,

    pub symbol_table: HashMap<String, DefId>,
}

#[derive(Clone, Copy, PartialEq, Eq, Debug)]
pub enum Lhs {
    Var(VarId),
    Pin(CellId, usize),
}

#[derive(Default, Debug)]
pub struct HGraph {
    pub vars: HashMap<VarId, HVar>,
    pub exprs: HashMap<ExprId, HExpr>,
    pub cells: HashMap<CellId, HCell>,
    pub conns: Vec<(ExprId, ExprId)>, // (lhs, rhs)

    pub symbol_table: HashMap<String, DefId>,

    expr_ty_map: RefCell<HashMap<ExprId, ty::Ty>>,

    var_counter: IdCounter,
    expr_counter: IdCounter,
    cell_counter: IdCounter,
}

#[derive(Clone, Copy, PartialEq, Eq, Hash)]
pub enum GlobalDefId {
    Mod(ModId),
    Func(FuncId),
}

impl std::fmt::Debug for GlobalDefId {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::Mod(x) => x.fmt(f),
            Self::Func(x) => x.fmt(f),
        }
    }
}

impl GlobalDefId {
    pub fn try_as_module(self) -> Option<ModId> {
        if let Self::Mod(x) = self {
            Some(x)
        } else {
            None
        }
    }
    pub fn try_as_function(self) -> Option<FuncId> {
        if let Self::Func(x) = self {
            Some(x)
        } else {
            None
        }
    }
}

impl From<ModId> for GlobalDefId {
    fn from(value: ModId) -> Self {
        GlobalDefId::Mod(value)
    }
}

impl From<FuncId> for GlobalDefId {
    fn from(value: FuncId) -> Self {
        GlobalDefId::Func(value)
    }
}

#[derive(Default, Debug)]
pub struct Design {
    mod_ty_map: HashMap<ModId, ty::ModTy>,
    fun_ty_map: HashMap<FuncId, ty::FuncTy>,
    mod_body_map: HashMap<ModId, RefCell<HGraph>>,
    symbol_table: HashMap<String, GlobalDefId>,
    def_counter: IdCounter,
}

#[derive(Debug)]
pub struct ItemRedefinedError;

impl Design {
    pub fn new_mod_id(&self) -> ModId {
        ModId(self.def_counter.new_id())
    }
    pub fn new_func_id(&self) -> FuncId {
        FuncId(self.def_counter.new_id())
    }

    pub fn set_module_type(
        &mut self,
        mid: ModId,
        name: &str,
        mod_ty: ty::ModTy,
    ) -> Result<(), ItemRedefinedError> {
        self.symbol_table
            .try_insert_(name.to_owned(), mid.into())
            .map_err(|_| ItemRedefinedError)?;
        self.mod_ty_map.try_insert_(mid, mod_ty).expect("ICE");
        Ok(())
    }

    pub fn set_function_type(
        &mut self,
        fid: FuncId,
        name: &str,
        fun_ty: ty::FuncTy,
    ) -> Result<(), ItemRedefinedError> {
        self.symbol_table
            .try_insert_(name.to_owned(), fid.into())
            .map_err(|_| ItemRedefinedError)?;
        self.fun_ty_map.try_insert_(fid, fun_ty).expect("ICE");
        Ok(())
    }

    pub fn set_module_body(&mut self, mid: ModId, body: HGraph) {
        self.mod_body_map
            .try_insert_(mid, RefCell::new(body))
            .expect("ICE")
    }

    pub fn resolve(&self, name: &str) -> Option<GlobalDefId> {
        self.symbol_table.get(name).cloned()
    }

    pub fn get_function_type(&self, fid: FuncId) -> &ty::FuncTy {
        &self.fun_ty_map[&fid]
    }
    pub fn get_module_type(&self, mid: ModId) -> &ty::ModTy {
        &self.mod_ty_map[&mid]
    }

    pub fn get_module_body(&self, mid: ModId) -> Ref<'_, HGraph> {
        self.mod_body_map[&mid].borrow()
    }

    pub fn get_module_body_mut(&self, mid: ModId) -> RefMut<'_, HGraph> {
        self.mod_body_map[&mid].borrow_mut()
    }
}

#[derive(Debug)]
pub struct RedefinedError;

#[derive(Clone, Copy, PartialEq, Eq, Debug)]
pub enum VarKind {
    In,
    Out,
    Local,
}

impl HGraph {
    pub fn new() -> Self {
        Self::default()
    }

    fn new_var_id(&self) -> VarId {
        VarId(self.var_counter.new_id())
    }

    fn new_expr_id(&self) -> ExprId {
        ExprId(self.expr_counter.new_id())
    }

    fn new_cell_id(&self) -> CellId {
        CellId(self.cell_counter.new_id())
    }

    fn symbol_redefined_check(&self, name: &str) -> Result<(), RedefinedError> {
        if self.symbol_table.contains_key(name) {
            return Err(RedefinedError);
        }

        Ok(())
    }

    pub fn add_var(
        &mut self,
        name: &str,
        kind: VarKind,
        ty: ty::Ty,
    ) -> Result<VarId, RedefinedError> {
        self.symbol_redefined_check(name)?;

        let id = self.new_var_id();
        self.symbol_table
            .insert_unwrap_(name.into(), DefId::Var(id));

        self.vars.insert_unwrap_(id, HVar { ty, kind });
        // TODO: handle kind

        Ok(id)
    }

    pub fn add_expr(&mut self, e: HExpr) -> ExprId {
        let id = self.new_expr_id();
        self.exprs.insert_unwrap_(id, e);
        id
    }

    pub fn add_conn(&mut self, lhs: ExprId, rhs: ExprId) {
        self.conns.push((lhs, rhs));
    }

    pub fn add_cell(&mut self, name: &str, mid: ModId) -> Result<CellId, RedefinedError> {
        self.symbol_redefined_check(name)?;

        let id = self.new_cell_id();
        self.symbol_table
            .insert_unwrap_(name.into(), DefId::Cell(id));

        self.cells.insert_unwrap_(id, HCell { mid });

        Ok(id)
    }
}

impl HGraph {
    pub fn lookup_var(&self, name: &str) -> Option<VarId> {
        match self.symbol_table.get(name) {
            Some(&DefId::Var(v)) => Some(v),
            _ => None,
        }
    }

    pub fn lookup_cell(&self, name: &str) -> Option<(CellId, ModId)> {
        match self.symbol_table.get(name) {
            Some(&DefId::Cell(c)) => {
                let mid = self.cells[&c].mid;
                Some((c, mid))
            }
            _ => None,
        }
    }
}

// #[derive(Debug)]
// struct HGraphChecked {
//     inner: HGraph,
// }

// impl HGraphChecked {
//     pub fn into_inner(self) -> HGraph {
//         self.inner
//     }
// }

// impl std::ops::Deref for HGraphChecked {
//     type Target = HGraph;

//     fn deref(&self) -> &Self::Target {
//         &self.inner
//     }
// }

pub fn check_design(de: &Design) {
    for (&_mid, body) in &de.mod_body_map {
        let mut body = body.borrow_mut();
        hgraph_infer_ty(de, &mut body);
    }
}

pub fn hgraph_infer_ty(de: &Design, g: &mut HGraph) {
    let mut expr_ty_map = g.expr_ty_map.borrow_mut();
    let mut inferer = TypeInferer {
        de,
        g,
        expr_ty_map: &mut expr_ty_map,
    };
    for &(lhs, rhs) in &g.conns {
        let lhs_ty = inferer.visit(lhs);
        let rhs_ty = inferer.visit(rhs);
        assert_eq!(lhs_ty, rhs_ty);
    }
}

struct TypeInferer<'de, 'g, 'm> {
    de: &'de Design,
    g: &'g HGraph,
    expr_ty_map: &'m mut HashMap<ExprId, Ty>, // borrowed from g.expr_ty_map
}

impl<'de, 'g, 'm> TypeInferer<'de, 'g, 'm> {
    fn visit(&mut self, expr: ExprId) -> Ty {
        let ty = self.visit_(expr);
        self.expr_ty_map.insert_unwrap_(expr, ty.clone());
        ty
    }

    fn visit_(&mut self, expr: ExprId) -> Ty {
        match &self.g.exprs[&expr] {
            &HExpr::Lit(ref v) => v.get_ty(),
            &HExpr::Pin(cid, pid) => {
                let mid = self.g.cells[&cid].mid;
                self.de.get_module_type(mid).get_port(pid).ty.clone()
            }
            &HExpr::Var(vid) => self.g.vars[&vid].ty.clone(),
            &HExpr::Uop(_op, opr1) => {
                let ty1 = self.visit(opr1);
                assert!(ty1.is_bit());
                Ty::mk_bit()
            }
            &HExpr::Binop(_op, opr1, opr2) => {
                let ty1 = self.visit(opr1);
                let ty2 = self.visit(opr2);
                assert!(ty1.is_bit());
                assert!(ty2.is_bit());
                Ty::mk_bit()
            }
            &HExpr::Tuple(ref opr_list) => {
                let mut sub_ty_list = vec![];
                for &opr in opr_list {
                    let ty = self.visit(opr);
                    sub_ty_list.push(ty)
                }
                Ty::mk_tuple(sub_ty_list)
            }

            &HExpr::List(ref opr_list) => {
                let mut width = 0;
                for &opr in opr_list {
                    let ty = self.visit(opr);
                    width += match ty.resolve() {
                        TyKind::Bit => 1,
                        TyKind::UnpackTuple(args) => {
                            assert!(args.iter().all(Ty::is_bit));
                            args.len()
                        }
                        _ => unimplemented!("CE"),
                    }
                }
                Ty::mk_bits(width)
            }

            &HExpr::Unpack(opr) => {
                let ty = self.visit(opr);
                match ty.resolve() {
                    TyKind::Bits(w) => Ty::mk_unpack_tuple_repeat(Ty::mk_bit(), *w),
                    TyKind::Tuple(args) => Ty::mk_unpack_tuple(args.clone()),
                    _ => unimplemented!("CE"),
                }
            }

            &HExpr::Index(opr, ref args) => {
                let ty = self.visit(opr);
                match ty.resolve() {
                    TyKind::Bits(_) => {}
                    _ => unimplemented!("CE"),
                }

                assert!(args.len() == 1);

                let arg0_ty = self.visit(args[0]);
                assert!(arg0_ty.is_int(), "CE");

                Ty::mk_bit()
            }

            &HExpr::Call(fid, ref args) => {
                use itertools::enumerate;

                let fun_ty = self.de.get_function_type(fid);

                assert!(args.len() == fun_ty.param_count());
                for (i, &arg) in enumerate(args) {
                    let arg_ty = self.visit(arg);
                    assert!(&arg_ty == fun_ty.param_ty(i));
                }

                fun_ty.ret.clone()
            }
        }
    }
}
