use std::path::{Path, PathBuf};

use unicode_width::UnicodeWidthChar;

pub struct Source {
    path: PathBuf,
    text: String,
}

impl Source {
    pub fn from_file(text: String, path: PathBuf) -> Self {
        Source { path, text }
    }
    pub fn text(&self) -> &str {
        &self.text
    }
    pub fn file_path(&self) -> &Path {
        &self.path
    }
    pub fn pos_from_byte_offset(&self, offset: usize) -> TextPos {
        let mut row = 0;
        let mut col = 0;
        for c in self.text[..offset].chars() {
            if c == '\n' {
                row += 1;
                col = 0;
            } else {
                col += c.width().unwrap_or(0);
            }
        }
        TextPos {
            row: row + 1,
            column: col + 1,
        }
    }
}

#[derive(Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash, Debug)]
pub struct TextPos {
    pub row: usize,    // one-based
    pub column: usize, // one-based
}
