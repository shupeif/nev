use std::path::{Path, PathBuf};

use anyhow::Context;
use clap::Parser as _;
use nev::diag::DiagImpl;
use nev_ast::parse::ParseError;
use nev_core as nev;
use util::Source;

mod util;

#[derive(Debug, clap::Parser)]
pub struct App {
    #[clap(subcommand)]
    command: Command,
}

#[derive(Debug, clap::Subcommand)]
enum Command {
    Run(RunArgs),
    Check(CheckArgs),
}

#[derive(Debug, clap::Args)]
struct RunArgs {
    path: PathBuf,

    #[arg(last = true)]
    extra_args: Vec<String>,
}

#[derive(Debug, clap::Args)]
struct CheckArgs {
    path: PathBuf,

    #[arg(long)]
    dump_ast: bool,
    #[arg(long)]
    dump_hir: bool,
    #[arg(long)]
    dump_thir: bool,
}

fn main() -> anyhow::Result<()> {
    let app = App::parse();
    // dbg!(&app);

    init_logging();
    match &app.command {
        Command::Run(args) => command_run(args),
        Command::Check(args) => command_check(args),
    }
}

fn command_run(args: &RunArgs) -> anyhow::Result<()> {
    let _text = read_source(&args.path)?;

    // TODO: CLI interface supports passing extra args, but how to deal with it?
    if !args.extra_args.is_empty() {
        log::warn!("Passing args to script is an incomplete feature.");
    }

    log::error!("Running scripts is not implemented");
    Ok(())
}

fn map_tokenization_error(e: ParseError, text: &Source) -> anyhow::Error {
    let pos = text.pos_from_byte_offset(e.loc);
    let context = format!(
        "At {}:{}:{}",
        text.file_path().display(),
        pos.row,
        pos.column,
    );
    anyhow::Error::new(e).context(context)
}

fn map_parse_error(e: Box<DiagImpl>, text: &Source) -> anyhow::Error {
    if let Some((span_start, _span_end)) = e.span {
        let pos = text.pos_from_byte_offset(span_start);
        anyhow::Error::new(e).context(format!(
            "At {}:{}:{}",
            text.file_path().display(),
            pos.row,
            pos.column,
        ))
    } else {
        anyhow::Error::new(e)
    }
}

fn command_check(args: &CheckArgs) -> anyhow::Result<()> {
    let text = read_source(&args.path)?;

    let ast = nev_ast::parse::parse(text.text()).map_err(|e| map_tokenization_error(e, &text))?;
    if args.dump_ast {
        dbg!(&ast);
    }

    let hir = nev::lower::lower_design(ast.root()).map_err(|e| map_parse_error(e, &text))?;
    if args.dump_hir {
        dbg!(&hir);
    }

    nev::hir::check_design(&hir);
    if args.dump_thir {
        dbg!(&hir);
    }

    Ok(())
}

fn init_logging() {
    use simplelog::*;
    TermLogger::init(
        LevelFilter::Info,
        Config::default(),
        TerminalMode::Stderr,
        ColorChoice::Auto,
    )
    .unwrap();
}

fn read_source(path: &Path) -> anyhow::Result<Source> {
    let text = std::fs::read_to_string(path)
        .with_context(|| format!("Failed to read source file: {}", path.display()))?;

    Ok(Source::from_file(text, path.into()))
}

// fn read_file_to_string(path: impl AsRef<Path>) -> anyhow::Result<String> {
//     let path = path.as_ref();
//     std::fs::read_to_string(path)
//         .with_context(|| format!("Failed to read from '{}'", path.display()))
// }

// fn main2() {
//     let ast = nev::parse::parse(SOURCE).unwrap();
//     let design = nev::lower_hir::lower_design(ast.root()).unwrap();
//     let mid = design.resolve_module("M").unwrap();
//     {
//         let mut g = design.get_module_body_mut(mid);
//         nev::hir::hgraph_infer_ty(&design, &mut g);
//     }
//     println!("{:#?}", design);
// }
