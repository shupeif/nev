use std::collections::HashMap;

use pyo3::{exceptions::PyValueError, prelude::*};

use nev::ModId;
use nev_core as nev;

#[pyclass]
struct RawDesign {
    inner: nev::Design,
}

impl RawDesign {
    fn graph(&self, mid: u32) -> &nev::Graph {
        &self.inner.graphs[&ModId(mid)]
    }
    fn compact_graph(&self, mid: u32) -> &nev::CompactGraph {
        &self.inner.compact_graphs[&ModId(mid)]
    }
}

#[pymethods]
impl RawDesign {
    #[getter]
    fn module_count(&self) -> usize {
        self.inner.name_map.len()
    }

    fn get_module_id(&self, name: &str) -> Option<u32> {
        self.inner.name_map.get(name).map(|x| x.0)
    }

    fn get_module_name_map(&self) -> Vec<(u32, String)> {
        self.inner
            .name_map
            .iter()
            .map(|(name, mod_id)| (mod_id.0, name.clone()))
            .collect()
    }

    fn module_get_graph_debug_text(&self, mid: u32) -> String {
        format!("{:#?}", self.graph(mid))
    }

    fn module_get_compact_graph_debug_text(&self, mid: u32) -> String {
        format!("{:#?}", self.compact_graph(mid))
    }

    fn module_get_input_count(&self, mid: u32) -> usize {
        self.compact_graph(mid).input_count()
    }

    fn module_get_output_count(&self, mid: u32) -> usize {
        self.compact_graph(mid).input_count()
    }

    fn module_get_raw_cell_stat(&self, mid: u32) -> HashMap<String, usize> {
        let mut cell_count_map = HashMap::new();

        for cell in self.graph(mid).cells.values() {
            let ty_name = cell.ty_name();
            *cell_count_map.entry(ty_name).or_default() += 1;
        }

        cell_count_map
    }

    fn module_eval(&self, mid: u32, inputs: Vec<i32>) -> Vec<i32> {
        let module = self.compact_graph(mid);

        let inputs = Vec::from_iter(inputs.iter().map(|x| match x {
            0 => false,
            1 => true,
            _ => panic!(),
        }));

        let output_count = module.output_count();
        let mut outputs = vec![false; output_count];
        self.inner.eval(ModId(mid), &inputs, &mut outputs);

        Vec::from_iter(outputs.iter().map(|x| match x {
            false => 0,
            true => 1,
        }))
    }
}

#[pyfunction]
fn compile<'py>(source: &str, opt_level: usize) -> PyResult<RawDesign> {
    let ast = nev_ast::parse::parse(source).map_err(|e| PyValueError::new_err(format!("{e:?}")))?;
    let mut design = nev::lower_design(ast.root());

    design.compile(opt_level);

    Ok(RawDesign { inner: design })
}

#[pymodule]
fn nev_raw(_py: Python<'_>, m: &PyModule) -> PyResult<()> {
    m.add_class::<RawDesign>()?;
    m.add_function(wrap_pyfunction!(compile, m)?)?;
    Ok(())
}
