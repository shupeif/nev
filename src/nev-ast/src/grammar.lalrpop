use super::TokenKind as TK;
use super::grammar_helper::*;
use super::grammar_helper::WithBuilder;

grammar<'input, 'db>(
    input: &'input str,
    db: &'db Builder,
);

pub Design: &'db Design<'db> = {
    <item_list: List<PkgItem>> => Design{<>}.with(db),
}

PkgItem: PkgItem<'db> = {
    Module => PkgItem::Module(<>),
    Function => PkgItem::Function(<>),
}

Module: &'db Module<'db> = {
    <attr: Attr?>
    "module" <name: Ident>
    "{" <stmt_list: List<Stmt>> "}"
    => Module{<>}.with(db),
}

Param: &'db Param<'db> = {
    <name: Ident> <ty: (":" <Expr>)?> => Param{<>}.with(db),
}

Function: &'db Function<'db> = {
    <attr: Attr?>
    "func" <name: Ident>
    <generic_param_list: ("<" <ListComma<Param>> ">")?>
    "(" <param_list: ListComma<Param>> ")"
    "->" <ret_ty: Expr>
    <body: OptionalFuncBody>
    => Function{<>}.with(db),
}

OptionalFuncBody: Option<&'db FunctionBody<'db>> = {
    ";" => None,
    "{"
        <stmt_list: List<FuncStmt>>
        <ret_expr: Expr>
    "}" => Some(FunctionBody{<>}.with(db))
}

Stmt: Stmt<'db> = {
    <kind: SigKind> <name: Ident> <ty: (":" <Expr>)?> <body: ("=" <Expr>)?> ";" => {
       Stmt::SigDecl(SigDecl{<>}.with(db))
    },
    <kind: BitSigKind>  <vars: ListComma<Ident>> ";" => {
        Stmt::BitSigDecl(BitSigDecl{<>}.with(db))
    },
    "cell" <vars: ListComma<Ident>> ":" <ty: Ident> ";" => {
        Stmt::CellDecl(CellDecl{<>}.with(db))
    },
    Assign => Stmt::Assign(<>),
}

SigKind: SigKind = {
    "input" => SigKind::Input,
    "output" => SigKind::Output,
    "wire" => SigKind::Wire,
}

BitSigKind: SigKind = {
    "binput" => SigKind::Input,
    "boutput" => SigKind::Output,
    "bwire" => SigKind::Wire,
}

#[inline]
FuncStmt: FuncStmt<'db> = {
    LetStmt => FuncStmt::Let(<>),
    Assign => FuncStmt::Assign(<>),
    IfStmt => FuncStmt::If(<>),
}

LetStmt: &'db LetStmt<'db> = {
    "let" <pat: Expr> "=" <body: Expr> ";"
    => LetStmt{<>}.with(db),
}

Assign: &'db Assign<'db> = {
    <lhs: Expr> ":=" <rhs: Expr> ";"
    => Assign{<>}.with(db),
}

IfStmt: &'db IfStmt<'db> = {
    "if" <cond: Expr> <if_: StmtBlock>
    <else_: ("else" <StmtBlock>)?>
    => IfStmt{<>}.with(db),
}

StmtBlock: StmtBlock<'db> = {
    "{" <stmt_list: List<FuncStmt>> "}" => StmtBlock{<>},
}

#[inline]
ListCommaTuple<T>: PVec<'db, T> = {
    CommaTuple<T> => db.alloc_slice(&<>),
}

// used in tuple syntax
CommaTuple<T>: Vec<T> = {
    <vs: (<T> ",")+> => vs,
    <vs: (<T> ",")+> <v: T> => vec_push(vs, v),
}

#[inline]
ListComma<T>: PVec<'db, T> = {
    Comma<T> => db.alloc_slice(&<>),
}

Comma<T>: Vec<T> = {
    <vs: (<T> ",")*> => vs,
    <vs: (<T> ",")*> <v: T> => vec_push(vs, v),
}

#[inline]
List<T>: PVec<'db, T> = {
    T* => db.alloc_slice(&<>),
}

Expr = Expr2;

Expr0: PExpr<'db> = {
    "(" <Expr> ")"=> Expr::Paren(<>).with(db),
    <Number> => Expr::IntLit(<>).with(db),
    <StringLit> => Expr::StringLit(<>).with(db),
    <Ident> => Expr::Var(<>).with(db),
    <bit0> => Expr::BitLit(false).with(db),
    <bit1> => Expr::BitLit(true).with(db),

    "(" <args: ListCommaTuple<Expr>> ")" => Expr::Tuple(<>).with(db),
    "[" <args: ListComma<Expr>> "]" => Expr::List(<>).with(db),

    <obj: Expr0> "." <field: Ident> => Expr::Dot(<>).with(db),
    <obj: Ident> "(" <args: ListComma<Expr>> ")" => Expr::Call(<>).with(db),
    <obj: Expr0> "[" <args: ListComma<Expr>> "]" => Expr::Index(<>).with(db),
    <obj: Expr0> "<" <args: ListComma<Expr>> ">" => Expr::ApplyGeneric(<>).with(db),

    "tuple" "<" <args:ListComma<Expr>> ">" => Expr::TypeTuple(args).with(db),
}

Expr1: PExpr<'db> = {
    <Expr0>,
    "~" <Expr1> => db.mk_uop_expr(Uop::Not, <>),
    "*" <Expr1> => Expr::Unpack(<>).with(db),
}

Expr2: PExpr<'db> = {
    <Expr1>,
    <Expr2> "&" <Expr1> => db.mk_binop_expr(Bop::And, <>),
    <Expr2> "|" <Expr1> => db.mk_binop_expr(Bop::Or, <>),
    <Expr2> "^" <Expr1> => db.mk_binop_expr(Bop::Xor, <>),

    <Expr1> "&~" <Expr1> => db.mk_binop_expr(Bop::Nand, <>),
    <Expr1> "|~" <Expr1> => db.mk_binop_expr(Bop::Nor, <>),
    <Expr1> "^~" <Expr1> => db.mk_binop_expr(Bop::Xnor, <>),
}

Attr: &'db Attr<'db> = "#" "[" <attrs: ListComma<AttrSeg>> "]" => Attr{<>}.with(db);

AttrSeg: &'db AttrSeg<'db> = {
    <name: Ident> <expr: ("=" <Expr>)?> => AttrSeg{<>}.with(db),
}

StringLit: StringLit<'db> = start_double_quote <l: @L> <x: string_seg?> <r: @R> end_double_quote => db.mk_lit_string(&input[l..r], l..r);
Number: IntLit<'db> = <l: @L> <x: number> <r: @R> => db.mk_lit_number(&input[l..r]);
Ident: Ident<'db> = <l: @L> <x: ident> <r: @R> => db.mk_ident(&input[l..r], l..r);

extern {
    type Location = usize;
    type Error = std::convert::Infallible;

    enum TK {
        "(" => TK::LeftParen,
        ")" => TK::RightParen,
        "[" => TK::LeftBracket,
        "]" => TK::RightBracket,
        "{" => TK::LeftBrace,
        "}" => TK::RightBrace,
        "<" => TK::LeftAngle,
        ">" => TK::RightAngle,
        "," => TK::Comma,
        ";" => TK::Semicolon,
        ":" => TK::Colon,
        "." => TK::Dot,
        "*" => TK::Star,
        "#" => TK::Hash,

        start_double_quote => TK::DoubleQuoteStart,
        end_double_quote => TK::DoubleQuoteEnd,
        string_seg => TK::LitStringSeg,
        

        number => TK::Number,
        ident => TK::Ident,

        bit0 => TK::Bit0,
        bit1 => TK::Bit1,

        "=" => TK::Equal,
        ":=" => TK::Assign,
        "->" => TK::Arrow,
        "~" => TK::Not,
        "&" => TK::And,
        "|" => TK::Or,
        "^" => TK::Xor,
        "&~" => TK::Nand,
        "|~" => TK::Nor,
        "^~" => TK::Xnor,

        "module" => TK::KwModule,
        "func" => TK::KwFunc, 
        "binput" => TK::KwBInput,
        "boutput" => TK::KwBOutput,
        "bwire" => TK::KwBWire,
        "input" => TK::KwInput,
        "output" => TK::KwOutput,
        "wire" => TK::KwWire,
        "reg" => TK::KwReg,
        "cell" => TK::KwCell,
        "let" => TK::KwLet,
        "tuple" => TK::KwTuple,

        "if" => TK::KwIf,
        "elif" => TK::KwElif,
        "else" => TK::KwElse,
    }
}