use std::fmt::{Debug, Formatter};

pub type P<'a, T> = &'a T;
pub type POption<'a, T> = Option<&'a T>;
pub type PPVec<'a, T> = &'a [&'a T];
pub type PVec<'a, T> = &'a [T];
pub type PStr<'a> = &'a str;

#[derive(Debug)]
pub struct Design<'a> {
    pub item_list: PVec<'a, PkgItem<'a>>,
}

#[derive(Clone, Copy)]
pub enum PkgItem<'a> {
    Module(&'a Module<'a>),
    Function(&'a Function<'a>),
}

impl<'a> Debug for PkgItem<'a> {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::Module(arg0) => arg0.fmt(f),
            Self::Function(arg0) => arg0.fmt(f),
        }
    }
}

#[derive(Debug)]
pub struct Function<'a> {
    pub attr: POption<'a, Attr<'a>>,
    pub name: Ident<'a>,
    pub generic_param_list: Option<PPVec<'a, Param<'a>>>,
    pub param_list: PPVec<'a, Param<'a>>,
    pub ret_ty: PExpr<'a>,
    pub body: POption<'a, FunctionBody<'a>>,
}

#[derive(Debug)]
pub struct Param<'a> {
    pub name: Ident<'a>,
    pub ty: Option<PExpr<'a>>,
}

#[derive(Debug)]
pub struct FunctionBody<'a> {
    pub stmt_list: PVec<'a, FuncStmt<'a>>,
    pub ret_expr: PExpr<'a>,
}

#[derive(Clone, Copy)]
pub enum FuncStmt<'a> {
    Let(&'a LetStmt<'a>),
    Assign(&'a Assign<'a>),
    If(&'a IfStmt<'a>),
}

impl<'a> Debug for FuncStmt<'a> {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::Let(x) => x.fmt(f),
            Self::Assign(x) => x.fmt(f),
            Self::If(x) => x.fmt(f),
        }
    }
}

#[derive(Debug)]
pub struct LetStmt<'a> {
    pub pat: PExpr<'a>,
    pub body: PExpr<'a>,
}

#[derive(Debug)]
pub struct IfStmt<'a> {
    pub cond: PExpr<'a>,
    pub if_: StmtBlock<'a>,
    pub else_: Option<StmtBlock<'a>>,
}

#[derive(Debug, Clone, Copy)]
pub struct StmtBlock<'a> {
    pub stmt_list: PVec<'a, FuncStmt<'a>>,
}

#[derive(Debug)]
pub struct Module<'a> {
    pub attr: POption<'a, Attr<'a>>,
    pub name: Ident<'a>,
    pub stmt_list: PVec<'a, Stmt<'a>>,
}

#[derive(Clone, Copy)]
pub enum Stmt<'a> {
    BitSigDecl(&'a BitSigDecl<'a>),
    SigDecl(&'a SigDecl<'a>),
    CellDecl(&'a CellDecl<'a>),
    Assign(&'a Assign<'a>),
}

impl<'a> std::fmt::Debug for Stmt<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::BitSigDecl(arg0) => arg0.fmt(f),
            Self::SigDecl(x) => x.fmt(f),
            Self::CellDecl(arg0) => arg0.fmt(f),
            Self::Assign(arg0) => arg0.fmt(f),
        }
    }
}

#[derive(Clone, Copy, PartialEq, Eq, Debug)]
pub enum SigKind {
    Input,
    Output,
    Wire,
    // Reg,
}

#[derive(Debug)]
pub struct SigDecl<'a> {
    pub kind: SigKind,
    pub name: Ident<'a>,
    pub ty: Option<PExpr<'a>>,
    pub body: Option<PExpr<'a>>,
}

#[derive(Debug)]
pub struct BitSigDecl<'a> {
    pub kind: SigKind,
    pub vars: PVec<'a, Ident<'a>>,
}

#[derive(Debug)]
pub struct CellDecl<'a> {
    pub ty: Ident<'a>,
    pub vars: PVec<'a, Ident<'a>>,
}

#[derive(Debug)]
pub struct Assign<'a> {
    pub lhs: PExpr<'a>,
    pub rhs: PExpr<'a>,
}

#[derive(Clone, Copy, PartialEq, PartialOrd, Debug)]
pub enum Binop {
    And,
    Or,
    Xor,
    Nand,
    Nor,
    Xnor,
}

#[derive(Clone, Copy, PartialEq, PartialOrd, Debug)]
pub enum Uop {
    Not,
    Neg,
}

pub type PExpr<'a> = P<'a, Expr<'a>>;

#[derive(Debug)]
pub enum Expr<'a> {
    Paren(PExpr<'a>),
    BitLit(bool),
    IntLit(IntLit<'a>),
    StringLit(StringLit<'a>),

    Var(Ident<'a>),
    Unpack(PExpr<'a>),
    Uop(Uop, PExpr<'a>),
    Binop(Binop, PExpr<'a>, PExpr<'a>),
    Tuple(PVec<'a, PExpr<'a>>),
    List(PVec<'a, PExpr<'a>>),
    Dot(PExpr<'a>, Ident<'a>),
    Call(Ident<'a>, PVec<'a, PExpr<'a>>),
    Index(PExpr<'a>, PVec<'a, PExpr<'a>>),
    ApplyGeneric(PExpr<'a>, PVec<'a, PExpr<'a>>),
    TypeTuple(PVec<'a, PExpr<'a>>),
}

#[derive(Clone, Copy, Debug)]
pub struct IntLit<'a> {
    pub raw_text: PStr<'a>,
}

#[derive(Debug)]
pub struct Attr<'a> {
    pub attrs: PPVec<'a, AttrSeg<'a>>,
}

#[derive(Debug)]
pub struct AttrSeg<'a> {
    pub name: Ident<'a>,
    pub expr: Option<PExpr<'a>>,
}

#[derive(Clone, Copy, Debug)]
pub struct Ident<'a> {
    pub span: (usize, usize),
    pub text: PStr<'a>,
}

#[derive(Clone, Copy, Debug)]
pub struct StringLit<'a> {
    pub raw_text: PStr<'a>,
}

pub trait AstNode: Debug {}

impl<'a> AstNode for Design<'a> {}
impl<'a> AstNode for Function<'a> {}
impl<'a> AstNode for Param<'a> {}
impl<'a> AstNode for FunctionBody<'a> {}
impl<'a> AstNode for Module<'a> {}
impl<'a> AstNode for SigDecl<'a> {}
impl<'a> AstNode for BitSigDecl<'a> {}
impl<'a> AstNode for CellDecl<'a> {}
impl<'a> AstNode for Assign<'a> {}
impl<'a> AstNode for LetStmt<'a> {}
impl<'a> AstNode for IfStmt<'a> {}
impl<'a> AstNode for StmtBlock<'a> {}
impl<'a> AstNode for Attr<'a> {}
impl<'a> AstNode for AttrSeg<'a> {}
impl<'a> AstNode for Expr<'a> {}
impl<'a> AstNode for Ident<'a> {}
