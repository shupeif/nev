use std::{convert::Infallible, ops::Range, rc::Rc};

#[derive(Clone, Debug)]
pub struct Token {
    pub kind: TokenKind,
    pub loc: Range<usize>,
}

struct Tokenizer<'a> {
    input: &'a [u8],
    state: TokenizerState,
    start: usize,
    idx: usize,
    tokens: Vec<Token>,
}

#[derive(Clone, Copy, PartialEq, Eq, Hash, Debug)]
enum TokenizerState {
    Default,
    StringLiteral,
}

pub struct InnerError;

impl Tokenizer<'_> {
    fn cur(&self) -> u8 {
        match self.input.get(self.idx) {
            Some(&c) => c,
            None => b'\0',
        }
    }
    fn peek(&self, offset: usize) -> u8 {
        match self.input.get(self.idx + offset) {
            Some(&c) => c,
            None => b'\0',
        }
    }
    fn cur_text(&self) -> &[u8] {
        &self.input[self.start..self.idx]
    }
    fn rest_text(&self) -> &[u8] {
        &self.input[self.idx..]
    }
    fn push_token(&mut self, kind: TokenKind) {
        self.tokens.push(Token {
            kind,
            loc: self.start..self.idx,
        })
    }
    fn skip_ws(&mut self) {
        loop {
            match self.cur() {
                c if c.is_ascii_whitespace() => {
                    self.idx += 1;
                }

                b'/' if self.peek(1) == b'/' => {
                    self.idx += 2;
                    loop {
                        match self.cur() {
                            b'\0' => break,
                            b'\n' => {
                                self.idx += 1;
                                break;
                            }

                            _ => {
                                self.idx += 1;
                            }
                        }
                    }
                }

                _ => break,
            }
        }
    }
    fn skip_shebang(&mut self) {
        if self.rest_text().starts_with(b"#!") {
            self.idx += 2;
            loop {
                match self.cur() {
                    0 => break,
                    b'\n' => {
                        self.idx += 1;
                        break;
                    }

                    _ => {}
                }
                self.idx += 1;
            }
        }
    }

    fn tokenize_next_default(&mut self) -> Result<(), InnerError> {
        // skip_ws shall be called before this function

        debug_assert_eq!(self.state, TokenizerState::Default);

        let kind;
        self.start = self.idx;

        fn _is_symbol(c: u8) -> bool {
            matches!(
                c,
                b'.' | b':' | b'=' | b'+' | b'-' | b'*' | b'&' | b'|' | b'^' | b'~' | b'<' | b'>'
            )
        }

        match self.cur() {
            b'\0' => {
                return Ok(());
            }
            b'(' => {
                kind = TokenKind::LeftParen;
                self.idx += 1;
            }
            b')' => {
                kind = TokenKind::RightParen;
                self.idx += 1;
            }
            b'[' => {
                kind = TokenKind::LeftBracket;
                self.idx += 1;
            }
            b']' => {
                kind = TokenKind::RightBracket;
                self.idx += 1;
            }
            b'{' => {
                kind = TokenKind::LeftBrace;
                self.idx += 1;
            }
            b'}' => {
                kind = TokenKind::RightBrace;
                self.idx += 1;
            }

            b',' => {
                kind = TokenKind::Comma;
                self.idx += 1;
            }
            b';' => {
                kind = TokenKind::Semicolon;
                self.idx += 1;
            }
            b'#' => {
                kind = TokenKind::Hash;
                self.idx += 1;
            }
            b'\"' => {
                kind = TokenKind::DoubleQuoteStart;
                self.idx += 1;
                self.state = TokenizerState::StringLiteral;
            }

            b'\'' => {
                self.idx += 1;

                kind = match self.cur() {
                    b'0' => TokenKind::Bit0,
                    b'1' => TokenKind::Bit1,
                    _ => return Err(InnerError {}),
                };
                self.idx += 1;
            }

            b'0'..=b'9' => {
                kind = TokenKind::Number;
                self.idx += 1;
                while matches!(self.cur(), b'0'..=b'9') {
                    self.idx += 1;
                }
            }

            b'a'..=b'z' | b'A'..=b'Z' | b'$' | b'_' => {
                // kind = Ident | Keyword
                self.idx += 1;
                while matches!(self.cur(), b'a'..=b'z' | b'A'..=b'Z' | b'0'..=b'9' | b'_') {
                    self.idx += 1;
                }
                kind = map_keyword(self.cur_text());
            }

            c if _is_symbol(c) => {
                self.idx += 1;
                while _is_symbol(self.cur()) {
                    self.idx += 1;
                }
                kind = map_op(self.cur_text()).ok_or(InnerError)?;
            }

            _ => return Err(InnerError),
        }

        self.push_token(kind);
        Ok(())
    }

    fn tokenize_next_string_literal(&mut self) -> Result<(), InnerError> {
        debug_assert_eq!(self.state, TokenizerState::StringLiteral);

        let kind;
        self.start = self.idx;

        fn _is_string_content(c: u8) -> bool {
            matches!(c,
                b'a'..=b'z' |
                b'A'..=b'Z' |
                b'0'..=b'9'
            )
        }

        match self.cur() {
            b'\"' => {
                self.idx += 1;
                kind = TokenKind::DoubleQuoteEnd;

                // "str"ident or "str""str" is forbidden, add a space between them
                if matches!(self.cur(), b'a'..=b'z' | b'A'..=b'Z' | b'$' | b'_' | b'\"') {
                    return Err(InnerError);
                }

                self.state = TokenizerState::Default;
            }

            c if _is_string_content(c) => {
                self.idx += 1;
                while _is_string_content(self.cur()) {
                    self.idx += 1;
                }
                kind = TokenKind::LitStringSeg;
            }

            b'\0' | b'\n' => return Err(InnerError), // unclosed string literal

            _ => return Err(InnerError),
        }

        self.push_token(kind);
        Ok(())
    }

    // success: tokens are stored in `self.tokens`
    // error: location of error could be retrieved from `self.index`
    fn tokenize(&mut self) -> Result<(), InnerError> {
        while self.idx < self.input.len() {
            let _start = self.idx;
            match self.state {
                TokenizerState::Default => {
                    self.skip_ws();
                    self.tokenize_next_default()?;
                }
                TokenizerState::StringLiteral => {
                    self.tokenize_next_string_literal()?;
                }
            }

            #[cfg(debug_assertions)]
            if _start == self.idx {
                log::error!("tokenization stuck: {:?}", self.tokens.last());
                return Err(InnerError);
            }
        }

        if self.state != TokenizerState::Default {
            return Err(InnerError);
        }
        Ok(())
    }
}

pub struct TokenizeError {
    pub index: usize,
    pub state: String,
}

fn tokenize(input: &str, skip_shebang: bool) -> Result<Vec<Token>, TokenizeError> {
    let mut tokenizer = Tokenizer {
        input: input.as_bytes(),
        state: TokenizerState::Default,
        start: 0,
        idx: 0,
        tokens: vec![],
    };

    if skip_shebang {
        tokenizer.skip_shebang();
    }
    tokenizer.tokenize().map_err(|_| TokenizeError {
        index: tokenizer.idx,
        state: format!("{:?}", tokenizer.state),
    })?;

    Ok(tokenizer.tokens)
}

#[derive(Clone)]
pub struct Ast {
    _store: Rc<bumpalo::Bump>,
    root: *const crate::ast::Design<'static>,
}

impl Ast {
    pub fn root(&self) -> &crate::ast::Design<'_> {
        unsafe { &*self.root }
    }
}

impl std::fmt::Debug for Ast {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        self.root().fmt(f)
    }
}

pub mod grammar_helper {
    pub use super::Ast;
    pub use crate::ast::*;

    pub use Binop as Bop;

    pub fn vec_push<T>(v: Vec<T>, d: T) -> Vec<T> {
        let mut v = v;
        v.push(d);
        v
    }

    #[derive(Default)]
    pub struct Builder {
        store: bumpalo::Bump,
    }

    impl Builder {
        pub unsafe fn finish_unchecked<'a>(self, root: *const Design<'a>) -> Ast {
            Ast {
                _store: std::rc::Rc::new(self.store),
                root: std::mem::transmute(root),
            }
        }
    }

    impl Builder {
        pub fn alloc<T>(&self, val: T) -> &T {
            self.store.alloc(val)
        }
        pub fn alloc_slice<T: Copy>(&self, src: &[T]) -> &[T] {
            self.store.alloc_slice_copy(src)
        }
        pub fn alloc_str(&self, src: &str) -> &str {
            self.store.alloc_str(src)
        }
    }

    pub(super) trait WithBuilder {
        type Output: ?Sized;
        fn with<'db>(self, db: &'db Builder) -> &'db Self::Output;
    }

    impl<T> WithBuilder for T
    where
        T: AstNode,
    {
        type Output = T;
        fn with<'db>(self, db: &'db Builder) -> &'db Self::Output {
            db.alloc(self)
        }
    }

    // impl<T> WithBuilder for Vec<T> where T: Copy {
    //     type Output = [T];
    //     fn with<'db>(self, db: &'db Builder) -> &'db Self::Output {
    //         db.alloc_slice(&self)
    //     }
    // }

    impl WithBuilder for &'_ str {
        type Output = str;

        fn with<'db>(self, db: &'db Builder) -> &'db Self::Output {
            db.alloc_str(self)
        }
    }

    impl<'a> Builder {
        pub fn mk_lit_number(&self, s: PStr) -> IntLit {
            IntLit {
                raw_text: self.alloc_str(s),
            }
        }
        pub fn mk_lit_string(&self, s: PStr, _span: std::ops::Range<usize>) -> StringLit {
            StringLit {
                raw_text: self.alloc_str(s),
            }
        }
        pub fn mk_ident(&self, s: PStr, span: std::ops::Range<usize>) -> Ident {
            Ident {
                span: (span.start, span.end),
                text: self.alloc_str(s),
            }
        }

        pub fn mk_binop_expr(&'a self, op: Binop, opr1: PExpr<'a>, opr2: PExpr<'a>) -> PExpr {
            self.alloc(Expr::Binop(op, opr1, opr2))
        }

        pub fn mk_uop_expr(&'a self, op: Uop, opr1: PExpr<'a>) -> PExpr {
            self.alloc(Expr::Uop(op, opr1))
        }
    }
}

use lalrpop_util::lalrpop_mod;

use crate::{
    token::{map_keyword, map_op},
    TokenKind,
};

lalrpop_mod!(pub grammar);

#[derive(Clone, Copy, PartialEq, Eq, Debug)]
pub enum ParseErrorReason {
    Tokenization,
    InvalidToken,
    UnrecognizedEof,
    UnrecognizedToken,
    ExtraToken,
}

#[derive(Debug)]
pub struct ParseError {
    pub reason: ParseErrorReason,
    pub loc: usize,
    pub token: Option<TokenKind>,
}

impl std::fmt::Display for ParseError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "Parse error at {} ({:?})", self.loc, self.reason)
    }
}

impl std::error::Error for ParseError {}

fn _map_tokenize_error(e: TokenizeError) -> ParseError {
    ParseError {
        reason: ParseErrorReason::Tokenization,
        loc: e.index,
        token: None,
    }
}

fn _map_parse_error(e: lalrpop_util::ParseError<usize, TokenKind, Infallible>) -> ParseError {
    use lalrpop_util::ParseError as LPE;
    use ParseErrorReason as PER;
    match e {
        LPE::InvalidToken { location } => ParseError {
            reason: PER::InvalidToken,
            loc: location,
            token: None,
        },
        LPE::UnrecognizedEOF {
            location,
            expected: _,
        } => ParseError {
            reason: PER::UnrecognizedEof,
            loc: location,
            token: None,
        },
        LPE::UnrecognizedToken {
            token: (l, token, _r),
            expected: _,
        } => ParseError {
            reason: PER::UnrecognizedToken,
            loc: l,
            token: Some(token),
        },
        LPE::ExtraToken {
            token: (l, token, _r),
        } => ParseError {
            reason: PER::ExtraToken,
            loc: l,
            token: Some(token),
        },
        LPE::User { error } => match error {},
    }
}

pub fn parse(input: &str) -> Result<Ast, ParseError> {
    use crate::ast::Design;

    let tokens = tokenize(input, true).map_err(_map_tokenize_error)?;
    let db = grammar_helper::Builder::default();
    let parser = grammar::DesignParser::new();
    let tokens = tokens.iter().map(|t| (t.loc.start, t.kind, t.loc.end));
    let root = parser.parse(input, &db, tokens).map_err(_map_parse_error)?;
    unsafe {
        let root = std::mem::transmute(root as *const Design);
        Ok(db.finish_unchecked(root))
    }
}
