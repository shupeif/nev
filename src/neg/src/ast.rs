use std::fmt::{Debug, Formatter};

pub type P<'a, T> = &'a T;
pub type POption<'a, T> = Option<&'a T>;
pub type PPVec<'a, T> = &'a [&'a T];
pub type PVec<'a, T> = &'a [T];
pub type PStr<'a> = &'a str;

#[derive(Debug)]
pub struct Top<'a> {
    pub item_list: PVec<'a, PkgItem<'a>>,
}

#[derive(Clone, Copy)]
pub enum PkgItem<'a> {
    Module(&'a Module<'a>),
}

impl<'a> Debug for PkgItem<'a> {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::Module(x) => x.fmt(f),
            // Self::Function(arg0) => arg0.fmt(f),
        }
    }
}

#[derive(Debug)]
pub struct Module<'a> {
    pub attr: POption<'a, Attr<'a>>,
    pub name: Ident<'a>,
    pub port_list: Option<PVec<'a, Ident<'a>>>,
    pub stmt_list: PVec<'a, ModItem<'a>>,
}

#[derive(Clone, Copy, PartialEq, Eq, Debug)]
pub enum PortDir {
    In,
    Out,
}

#[derive(Clone, Copy, PartialEq, Eq, Debug)]
pub enum SigKind {
    Input,
    Output,
    Wire,
    // Reg,
}

#[derive(Debug)]
pub struct Port<'a> {
    pub dir: Option<PortDir>,
    pub name: PStr<'a>,
    pub width: Option<IntLit<'a>>,
}

#[derive(Debug)]
pub struct ModuleBody<'a> {
    item_list: PVec<'a, ModItem<'a>>,
}

#[derive(Clone, Copy)]
pub enum ModItem<'a> {
    SigDecl(&'a SigDecl<'a>),
    Assign(&'a AssignStmt<'a>),
    Cell(&'a Cell<'a>),
}

impl Debug for ModItem<'_> {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::SigDecl(x) => x.fmt(f),
            Self::Assign(x) => x.fmt(f),
            Self::Cell(x) => x.fmt(f),
        }
    }
}

#[derive(Debug)]
pub struct SigDecl<'a> {
    pub kind: SigKind,
    pub vars: PVec<'a, Ident<'a>>,
}

#[derive(Debug)]
pub struct AssignStmt<'a> {
    pub lhs: Ident<'a>,
    pub rhs: Ident<'a>,
}

#[derive(Debug)]
pub struct Cell<'a> {
    pub module_name: Ident<'a>,
    pub name: Ident<'a>,
    pub conn_list: PPVec<'a, CellConn<'a>>,
}

#[derive(Debug)]
pub struct CellConn<'a> {
    pub pin: Ident<'a>,
    pub dir: PortDir,
    pub wire: Ident<'a>,
}

#[derive(Debug)]
pub struct Attr<'a> {
    pub attrs: PPVec<'a, AttrSeg<'a>>,
}

#[derive(Debug)]
pub struct AttrSeg<'a> {
    pub name: Ident<'a>,
    pub value: Option<Ident<'a>>,
}

#[derive(Clone, Copy, Debug)]
pub struct IntLit<'a> {
    pub raw_text: PStr<'a>,
}

#[derive(Clone, Copy, Debug)]
pub struct StringLit<'a> {
    pub raw_text: PStr<'a>,
}

#[derive(Clone, Copy)]
pub struct Ident<'a> {
    pub span: (usize, usize),
    pub text: PStr<'a>,
}

impl<'a> Debug for Ident<'a> {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{:?} @({}..{})", &self.text, self.span.0, self.span.1)
    }
}

pub trait AstNode: Debug {}

impl<'a> AstNode for Top<'a> {}
impl<'a> AstNode for PkgItem<'a> {}
impl<'a> AstNode for Module<'a> {}
impl<'a> AstNode for Port<'a> {}
impl<'a> AstNode for ModuleBody<'a> {}
impl<'a> AstNode for ModItem<'a> {}
impl<'a> AstNode for SigDecl<'a> {}
impl<'a> AstNode for AssignStmt<'a> {}
impl<'a> AstNode for Cell<'a> {}
impl<'a> AstNode for CellConn<'a> {}
impl<'a> AstNode for Attr<'a> {}
impl<'a> AstNode for AttrSeg<'a> {}
impl<'a> AstNode for IntLit<'a> {}
impl<'a> AstNode for StringLit<'a> {}
impl<'a> AstNode for Ident<'a> {}
