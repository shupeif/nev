#[derive(Clone, Copy, PartialEq, Eq, Debug)]
pub enum TokenKind {
    Eof,

    Ident,
    Number,

    Bit0,
    Bit1,

    DoubleQuoteStart,
    DoubleQuoteEnd,
    LitStringSeg,

    LeftParen,
    RightParen,
    LeftBracket,
    RightBracket,
    LeftBrace,
    RightBrace,
    LeftAngle,
    RightAngle,

    Comma,
    Semicolon,
    Hash,
    Colon,
    Dot,
    Star,
    Arrow,
    ColonLeftAngle,
    ColonRightAngle,

    Equal,
    Assign,
    Not,
    And,
    Or,
    Xor,
    Nand,
    Nor,
    Xnor,

    KwModule,
    KwFunc,
    KwInput,
    KwOutput,
    KwWire,
    KwReg,
    KwBInput,
    KwBOutput,
    KwBWire,
    KwCell,
    KwLet,
    KwTuple,

    KwIf,
    KwElif,
    KwElse,
}

pub(crate) fn map_keyword(input: &[u8]) -> TokenKind {
    match input {
        b"module" => TokenKind::KwModule,
        b"func" => TokenKind::KwFunc,
        b"binput" => TokenKind::KwBInput,
        b"boutput" => TokenKind::KwBOutput,
        b"bwire" => TokenKind::KwBWire,
        b"input" => TokenKind::KwInput,
        b"output" => TokenKind::KwOutput,
        b"wire" => TokenKind::KwWire,
        b"reg" => TokenKind::KwReg,
        b"cell" => TokenKind::KwCell,
        b"let" => TokenKind::KwLet,
        b"tuple" => TokenKind::KwTuple,

        b"if" => TokenKind::KwIf,
        b"elif" => TokenKind::KwElif,
        b"else" => TokenKind::KwElse,

        _ => TokenKind::Ident,
    }
}

pub(crate) fn map_op(input: &[u8]) -> Option<TokenKind> {
    Some(match input {
        // TODO: refine handling of left/right angle
        // current strategy is just a quick hack
        b"<" => TokenKind::LeftAngle,
        b">" => TokenKind::RightAngle,

        b"." => TokenKind::Dot,
        b":" => TokenKind::Colon,
        b"*" => TokenKind::Star,
        b"->" => TokenKind::Arrow,
        b"=" => TokenKind::Equal,
        b":=" => TokenKind::Assign,
        b":<" => TokenKind::ColonLeftAngle,
        b":>" => TokenKind::ColonRightAngle,

        _ => return None,
    })
}
