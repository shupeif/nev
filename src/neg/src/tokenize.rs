use crate::token::{map_keyword, map_op, TokenKind};

use std::ops::Range;

#[derive(Clone, Debug)]
pub struct Token {
    pub kind: TokenKind,
    pub loc: Range<usize>,
}

struct Tokenizer<'a> {
    input: &'a [u8],
    state: TokenizerState,
    start: usize,
    idx: usize,
    tokens: Vec<Token>,
}

#[derive(Clone, Copy, PartialEq, Eq, Hash, Debug)]
enum TokenizerState {
    Default,
    StringLiteral,
}

pub struct InnerError;

impl Tokenizer<'_> {
    fn cur(&self) -> u8 {
        match self.input.get(self.idx) {
            Some(&c) => c,
            None => b'\0',
        }
    }
    fn peek(&self, offset: usize) -> u8 {
        match self.input.get(self.idx + offset) {
            Some(&c) => c,
            None => b'\0',
        }
    }
    fn cur_text(&self) -> &[u8] {
        &self.input[self.start..self.idx]
    }
    fn rest_text(&self) -> &[u8] {
        &self.input[self.idx..]
    }
    fn push_token(&mut self, kind: TokenKind) {
        self.tokens.push(Token {
            kind,
            loc: self.start..self.idx,
        })
    }
    fn skip_ws(&mut self) {
        loop {
            match self.cur() {
                c if c.is_ascii_whitespace() => {
                    self.idx += 1;
                }

                b'/' if self.peek(1) == b'/' => {
                    self.idx += 2;
                    loop {
                        match self.cur() {
                            b'\0' => break,
                            b'\n' => {
                                self.idx += 1;
                                break;
                            }

                            _ => {
                                self.idx += 1;
                            }
                        }
                    }
                }

                _ => break,
            }
        }
    }

    fn tokenize_next_default(&mut self) -> Result<(), InnerError> {
        // skip_ws shall be called before this function

        debug_assert_eq!(self.state, TokenizerState::Default);

        let kind;
        self.start = self.idx;

        fn _is_symbol(c: u8) -> bool {
            matches!(
                c,
                b'.' | b':' | b'=' | b'+' | b'-' | b'*' | b'&' | b'|' | b'^' | b'~' | b'<' | b'>'
            )
        }

        match self.cur() {
            b'\0' => {
                return Ok(());
            }
            b'(' => {
                kind = TokenKind::LeftParen;
                self.idx += 1;
            }
            b')' => {
                kind = TokenKind::RightParen;
                self.idx += 1;
            }
            b'[' => {
                kind = TokenKind::LeftBracket;
                self.idx += 1;
            }
            b']' => {
                kind = TokenKind::RightBracket;
                self.idx += 1;
            }
            b'{' => {
                kind = TokenKind::LeftBrace;
                self.idx += 1;
            }
            b'}' => {
                kind = TokenKind::RightBrace;
                self.idx += 1;
            }

            b',' => {
                kind = TokenKind::Comma;
                self.idx += 1;
            }
            b';' => {
                kind = TokenKind::Semicolon;
                self.idx += 1;
            }
            b'#' => {
                kind = TokenKind::Hash;
                self.idx += 1;
            }
            b'\"' => {
                kind = TokenKind::DoubleQuoteStart;
                self.idx += 1;
                self.state = TokenizerState::StringLiteral;
            }

            b'\'' => {
                self.idx += 1;

                kind = match self.cur() {
                    b'0' => TokenKind::Bit0,
                    b'1' => TokenKind::Bit1,
                    _ => return Err(InnerError {}),
                };
                self.idx += 1;
            }

            b'0'..=b'9' => {
                kind = TokenKind::Number;
                self.idx += 1;
                while matches!(self.cur(), b'0'..=b'9') {
                    self.idx += 1;
                }
            }

            b'a'..=b'z' | b'A'..=b'Z' | b'$' | b'_' => {
                // kind = Ident | Keyword
                self.idx += 1;
                while matches!(self.cur(), b'a'..=b'z' | b'A'..=b'Z' | b'0'..=b'9' | b'_') {
                    self.idx += 1;
                }
                kind = map_keyword(self.cur_text());
            }

            c if _is_symbol(c) => {
                self.idx += 1;
                while _is_symbol(self.cur()) {
                    self.idx += 1;
                }
                kind = map_op(self.cur_text()).ok_or(InnerError)?;
            }

            _ => return Err(InnerError),
        }

        self.push_token(kind);
        Ok(())
    }

    fn tokenize_next_string_literal(&mut self) -> Result<(), InnerError> {
        debug_assert_eq!(self.state, TokenizerState::StringLiteral);

        let kind;
        self.start = self.idx;

        fn _is_string_content(c: u8) -> bool {
            matches!(c,
                b'a'..=b'z' |
                b'A'..=b'Z' |
                b'0'..=b'9'
            )
        }

        match self.cur() {
            b'\"' => {
                self.idx += 1;
                kind = TokenKind::DoubleQuoteEnd;

                // "str"ident or "str""str" is forbidden, add a space between them
                if matches!(self.cur(), b'a'..=b'z' | b'A'..=b'Z' | b'$' | b'_' | b'\"') {
                    return Err(InnerError);
                }

                self.state = TokenizerState::Default;
            }

            c if _is_string_content(c) => {
                self.idx += 1;
                while _is_string_content(self.cur()) {
                    self.idx += 1;
                }
                kind = TokenKind::LitStringSeg;
            }

            b'\0' | b'\n' => return Err(InnerError), // unclosed string literal

            _ => return Err(InnerError),
        }

        self.push_token(kind);
        Ok(())
    }

    // success: tokens are stored in `self.tokens`
    // error: location of error could be retrieved from `self.index`
    fn tokenize(&mut self) -> Result<(), InnerError> {
        while self.idx < self.input.len() {
            let _start = self.idx;
            match self.state {
                TokenizerState::Default => {
                    self.skip_ws();
                    self.tokenize_next_default()?;
                }
                TokenizerState::StringLiteral => {
                    self.tokenize_next_string_literal()?;
                }
            }

            #[cfg(debug_assertions)]
            if _start == self.idx {
                log::error!("tokenization stuck: {:?}", self.tokens.last());
                return Err(InnerError);
            }
        }

        if self.state != TokenizerState::Default {
            return Err(InnerError);
        }
        Ok(())
    }
}

pub struct TokenizeError {
    pub index: usize,
    pub state: String,
}

pub fn tokenize(input: &str) -> Result<Vec<Token>, TokenizeError> {
    let mut tokenizer = Tokenizer {
        input: input.as_bytes(),
        state: TokenizerState::Default,
        start: 0,
        idx: 0,
        tokens: vec![],
    };

    tokenizer.tokenize().map_err(|_| TokenizeError {
        index: tokenizer.idx,
        state: format!("{:?}", tokenizer.state),
    })?;

    Ok(tokenizer.tokens)
}
