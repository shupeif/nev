use clap::Parser;
use std::rc::Rc;

use emit::Emitter;
use store::{CellDef, Design, DesignAttr, DesignInterface, PortDir, Store};

mod ast;
mod emit;
mod emit_json;
mod parse;
mod store;
mod token;
mod tokenize;

#[derive(Parser)]
struct Args {
    input: String,
    #[arg(short, long)]
    output: String,
}

static SOURCE: &'static str = r#"
#[lib]
module NAND(A, B, ZN) {
    input  A, B;
    output ZN;
}
module NOT {
    input  A;
    output ZN;
    cell U = NAND(
        A  :< A,
        B  :< A,
        ZN :> ZN,
    );
}
"#;
fn main() {
    let args = Args::parse();
    let root = parse::parse(SOURCE).unwrap();
    let store = lower(root.root());

    // let mut buf = String::new();
    // let mut emitter = emit::Emitter::new(&mut buf);
    // {
    //     let mut e = emitter.emit_module("BUF", &["A", "Z"]);
    //     e.emit_input_port("A");
    //     e.emit_output_port("Z");
    //     e.emit_assign("Z", "A");
    //     e.finish();
    // }
    // {
    //     let mut e = emitter.emit_module("NOT", &["A", "ZN"]);
    //     e.emit_input_port("A");
    //     e.emit_output_port("ZN");
    //     {
    //         let mut e = e.emit_cell("NAND", "U");
    //         e.emit_conn("A", "A");
    //         e.emit_conn("B", "A");
    //         e.emit_conn("ZN", "ZN");
    //         e.finish();
    //     }
    //     e.finish();
    // }
    let mut buf = String::new();
    let mut e = Emitter::new(&mut buf);
    emit_top(&mut e, &store);
    println!("{}", buf);

    println!("{}", emit_json::emit_json(&store));
}

pub fn lower(root: &ast::Top) -> Store {
    let mut store = Store::new();
    let mut module_list = vec![];
    for item in root.item_list {
        match *item {
            ast::PkgItem::Module(m) => {
                let iface = Rc::new(collect_module_ports(m));
                store.add_module_iface(iface.clone());
                module_list.push((iface, m));
            }
        }
    }
    for (iface, m) in module_list {
        let m = lower_module(&store, iface, m);
        store.add_module(m);
    }

    store
}

fn lower_module(store: &Store, iface: Rc<DesignInterface>, m: &ast::Module) -> Design {
    let mut design = Design::new_with_iface(iface.clone());
    for item in m.stmt_list {
        match *item {
            ast::ModItem::SigDecl(decl) => {
                if !matches!(decl.kind, ast::SigKind::Wire) {
                    continue;
                }
                for var in decl.vars {
                    design.add_wire(var.text);
                }
            }
            _ => {}
        }
    }
    for item in m.stmt_list {
        match *item {
            ast::ModItem::SigDecl(..) => {}
            ast::ModItem::Assign(_assign) => {
                todo!()
            }
            ast::ModItem::Cell(cell) => {
                let module = store
                    .get_module_iface_by_name(cell.module_name.text)
                    .unwrap();
                let mut c = CellDef::new(module, cell.name.text);
                for &conn in cell.conn_list {
                    let wire = design.get_signal_id_by_name(conn.wire.text).unwrap();
                    c.add_conn(conn.pin.text, wire);
                }
                design.add_cell(c);
            }
        }
    }
    design
}

pub fn parse_module_attrs(attrs: &ast::Attr) -> DesignAttr {
    let mut res = DesignAttr::default();
    for &attr in attrs.attrs {
        if attr.value.is_some() {
            log::error!(
                "Attribute value is unsupported, for `{}={}`",
                attr.name.text,
                attr.value.unwrap().text,
            );
        }
        match attr.name.text {
            "lib" => {
                res.is_lib = true;
            }

            _ => log::error!("Unknown module attribute `{}`", attr.name.text),
        }
    }
    res
}

pub fn collect_module_ports(m: &ast::Module) -> DesignInterface {
    let attr = match m.attr {
        Some(attr) => parse_module_attrs(attr),
        None => DesignAttr::default(),
    };
    let mut iface = DesignInterface::new(m.name.text, attr);
    for item in m.stmt_list {
        match *item {
            ast::ModItem::SigDecl(decl) => {
                let dir = match decl.kind {
                    ast::SigKind::Input => PortDir::In,
                    ast::SigKind::Output => PortDir::Out,
                    ast::SigKind::Wire => continue,
                };
                for sig in decl.vars {
                    iface.add_port(sig.text, dir);
                }
            }
            ast::ModItem::Cell(..) | ast::ModItem::Assign(..) => {}
        }
    }
    iface
}

fn emit_top(e: &mut Emitter, store: &Store) {
    store.foreach_module(|m| {
        if !m.iface().attr_is_lib() {
            emit_module(e, m);
        }
    });
}

fn emit_module(e: &mut Emitter, m: &Design) {
    let iface = m.iface();
    let port_list: Vec<&str> = iface.ports().map(|def| def.name.as_str()).collect();
    let mut e = e.emit_module(iface.name(), &port_list);

    for port in iface.ports() {
        match port.dir {
            PortDir::In => e.emit_input_port(&port.name),
            PortDir::Out => e.emit_output_port(&port.name),
        }
    }
    for wire in &m.wire_list {
        e.emit_wire(&wire.name);
    }
    for cell in &m.cell_list {
        let mut e = e.emit_cell(cell.module.name(), &cell.name);
        for conn in &cell.conn {
            let pin = &cell.module.get_by_id(conn.0).name;
            let wire = m.get_signal_name_by_id(conn.1);
            e.emit_conn(pin, wire);
        }
        e.finish()
    }
    e.finish();
}
