#[derive(Clone)]
pub struct Ast {
    _store: Rc<bumpalo::Bump>,
    root: *const crate::ast::Top<'static>,
}

impl Ast {
    pub fn root(&self) -> &crate::ast::Top<'_> {
        unsafe { &*self.root }
    }
}

impl std::fmt::Debug for Ast {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        self.root().fmt(f)
    }
}

pub mod grammar_helper {
    pub use super::Ast;
    pub use crate::ast::*;

    pub fn vec_push<T>(v: Vec<T>, d: T) -> Vec<T> {
        let mut v = v;
        v.push(d);
        v
    }

    #[derive(Default)]
    pub struct Builder {
        store: bumpalo::Bump,
    }

    impl Builder {
        pub unsafe fn finish_unchecked<'a>(self, root: *const Top<'a>) -> Ast {
            Ast {
                _store: std::rc::Rc::new(self.store),
                root: std::mem::transmute(root),
            }
        }
    }

    impl Builder {
        pub fn alloc<T>(&self, val: T) -> &T {
            self.store.alloc(val)
        }
        pub fn alloc_slice<T: Copy>(&self, src: &[T]) -> &[T] {
            self.store.alloc_slice_copy(src)
        }
        pub fn alloc_str(&self, src: &str) -> &str {
            self.store.alloc_str(src)
        }
    }

    pub(super) trait WithBuilder {
        type Output: ?Sized;
        fn with<'db>(self, db: &'db Builder) -> &'db Self::Output;
    }

    impl<T> WithBuilder for T
    where
        T: AstNode,
    {
        type Output = T;
        fn with<'db>(self, db: &'db Builder) -> &'db Self::Output {
            db.alloc(self)
        }
    }

    // impl<T> WithBuilder for Vec<T> where T: Copy {
    //     type Output = [T];
    //     fn with<'db>(self, db: &'db Builder) -> &'db Self::Output {
    //         db.alloc_slice(&self)
    //     }
    // }

    impl WithBuilder for &'_ str {
        type Output = str;

        fn with<'db>(self, db: &'db Builder) -> &'db Self::Output {
            db.alloc_str(self)
        }
    }

    impl<'a> Builder {
        pub fn mk_lit_number(&self, s: PStr) -> IntLit {
            IntLit {
                raw_text: self.alloc_str(s),
            }
        }
        pub fn mk_lit_string(&self, s: PStr, _span: std::ops::Range<usize>) -> StringLit {
            StringLit {
                raw_text: self.alloc_str(s),
            }
        }
        pub fn mk_ident(&self, s: PStr, span: std::ops::Range<usize>) -> Ident {
            Ident {
                span: (span.start, span.end),
                text: self.alloc_str(s),
            }
        }
    }
}

use std::{convert::Infallible, rc::Rc};

use lalrpop_util::lalrpop_mod;

use crate::{token::TokenKind, tokenize::TokenizeError};

lalrpop_mod!(pub grammar);

#[derive(Clone, Copy, PartialEq, Eq, Debug)]
pub enum ParseErrorReason {
    Tokenization,
    InvalidToken,
    UnrecognizedEof,
    UnrecognizedToken,
    ExtraToken,
}

#[derive(Debug)]
pub struct ParseError {
    pub reason: ParseErrorReason,
    pub loc: usize,
    pub token: Option<TokenKind>,
}

impl std::fmt::Display for ParseError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "Parse error at {} ({:?})", self.loc, self.reason)
    }
}

impl std::error::Error for ParseError {}

fn _map_tokenize_error(e: TokenizeError) -> ParseError {
    ParseError {
        reason: ParseErrorReason::Tokenization,
        loc: e.index,
        token: None,
    }
}

fn _map_parse_error(e: lalrpop_util::ParseError<usize, TokenKind, Infallible>) -> ParseError {
    use lalrpop_util::ParseError as LPE;
    use ParseErrorReason as PER;
    match e {
        LPE::InvalidToken { location } => ParseError {
            reason: PER::InvalidToken,
            loc: location,
            token: None,
        },
        LPE::UnrecognizedEOF {
            location,
            expected: _,
        } => ParseError {
            reason: PER::UnrecognizedEof,
            loc: location,
            token: None,
        },
        LPE::UnrecognizedToken {
            token: (l, token, _r),
            expected: _,
        } => ParseError {
            reason: PER::UnrecognizedToken,
            loc: l,
            token: Some(token),
        },
        LPE::ExtraToken {
            token: (l, token, _r),
        } => ParseError {
            reason: PER::ExtraToken,
            loc: l,
            token: Some(token),
        },
        LPE::User { error } => match error {},
    }
}

pub fn parse(input: &str) -> Result<Ast, ParseError> {
    use crate::ast::Top;
    use crate::tokenize::tokenize;

    let tokens = tokenize(input).map_err(_map_tokenize_error)?;
    let db = grammar_helper::Builder::default();
    let parser = grammar::TopParser::new();
    let tokens = tokens.iter().map(|t| (t.loc.start, t.kind, t.loc.end));
    let root = parser.parse(input, &db, tokens).map_err(_map_parse_error)?;
    unsafe {
        let root = std::mem::transmute(root as *const Top);
        Ok(db.finish_unchecked(root))
    }
}
