use std::fmt::Write;

// const INDENT_SPACES: &'static str = std::str::from_utf8(&[b' '; 128]).unwrap();

enum InnerState {
    TopLevel,
}

struct RawEmitter<'buf> {
    state: InnerState,
    indent: usize,
    buf: &'buf mut String,
}

impl<'buf> RawEmitter<'buf> {
    fn emit_ident(&mut self) {
        self.buf
            .extend(std::iter::repeat(' ').take(4 * self.indent));
    }

    fn indent(&mut self) {
        self.indent += 1;
    }
    fn unindent(&mut self) {
        self.indent = self.indent.checked_sub(1).unwrap();
    }
    fn write_fmt(&mut self, args: std::fmt::Arguments<'_>) {
        self.buf.write_fmt(args).unwrap();
    }
    fn write_str(&mut self, s: &str) {
        self.buf.push_str(s);
    }
}

pub struct Emitter<'a> {
    raw: RawEmitter<'a>,
}

impl<'buf> Emitter<'buf> {
    pub fn new(buf: &'buf mut String) -> Self {
        Emitter {
            raw: RawEmitter {
                state: InnerState::TopLevel,
                indent: 0,
                buf,
            },
        }
    }

    pub fn emit_module(&mut self, name: &str, port_list: &[&str]) -> ModuleEmitter<'buf, '_> {
        ModuleEmitter::new(&mut self.raw, name, port_list)
        // self.raw.emit_ident();
        // write!(self.raw.buf, "endmodule : {name}\n");
    }
}

pub struct ModuleEmitter<'buf, 'a> {
    raw: &'a mut RawEmitter<'buf>,
}

impl<'buf, 'a> ModuleEmitter<'buf, 'a> {
    fn new(raw: &'a mut RawEmitter<'buf>, name: &str, port_list: &[&str]) -> Self {
        raw.emit_ident();
        write!(raw, "module {name}(");
        if let &[first, ref rest @ ..] = port_list {
            write!(raw, "{first}");
            for &x in rest {
                write!(raw, ", {x}");
            }
        }
        write!(raw, ");\n");

        raw.indent();
        ModuleEmitter { raw }
    }

    pub fn finish(self) {
        self.raw.unindent();
        self.raw.emit_ident();
        write!(self.raw, "endmodule\n");
    }

    fn emit_port_(&mut self, dir: &str, port: &str) {
        let raw = &mut self.raw;
        raw.emit_ident();
        write!(raw, "{dir:<8}{port};\n");
    }
    fn emit_ports_(&mut self, dir: &str, port_list: &[&str]) {
        let raw = &mut self.raw;
        if let &[first, ref rest @ ..] = port_list {
            raw.emit_ident();
            write!(raw, "{dir:<8}{first}");
            for &x in rest {
                write!(raw, ", {x}");
            }
            write!(raw, ";\n");
        }
    }
    pub fn emit_input_port(&mut self, port: &str) {
        self.emit_port_("input", port);
    }
    pub fn emit_output_port(&mut self, port: &str) {
        self.emit_port_("output", port);
    }
    pub fn emit_input_ports(&mut self, port_list: &[&str]) {
        self.emit_ports_("input", port_list);
    }
    pub fn emit_output_ports(&mut self, port_list: &[&str]) {
        self.emit_ports_("output", port_list);
    }
    pub fn emit_wire(&mut self, port: &str) {
        self.emit_port_("wire", port);
    }
    pub fn emit_wires(&mut self, port_list: &[&str]) {
        self.emit_ports_("wire", port_list);
    }

    pub fn emit_assign(&mut self, lhs: &str, rhs: &str) {
        let raw = &mut self.raw;
        raw.emit_ident();
        write!(raw, "assign  {lhs} = {rhs};\n");
    }

    pub fn emit_cell(&mut self, mod_name: &str, cell_name: &str) -> CellEmitter<'buf, '_> {
        CellEmitter::new(self.raw, mod_name, cell_name)
    }
}

pub struct CellEmitter<'buf, 'a> {
    raw: &'a mut RawEmitter<'buf>,
    port_count: usize,
}

impl<'buf, 'a> CellEmitter<'buf, 'a> {
    fn new(raw: &'a mut RawEmitter<'buf>, mod_name: &str, cell_name: &str) -> Self {
        raw.emit_ident();
        write!(raw, "{mod_name} {cell_name}(");
        CellEmitter { raw, port_count: 0 }
    }

    pub fn finish(self) {
        let raw = self.raw;
        write!(raw, ");\n");
    }

    pub fn emit_conn(&mut self, port: &str, wire: &str) {
        let raw = &mut self.raw;
        if self.port_count != 0 {
            write!(raw, ", ");
        }
        self.port_count += 1;
        write!(raw, ".{port}({wire})");
    }
}
