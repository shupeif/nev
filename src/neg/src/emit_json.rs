use serde::{Serialize, Serializer};

use crate::store::{Design, PortDir, Store};

#[derive(Serialize)]
struct JsonDesign {
    name: String,
    ports: Vec<JsonPort>,
    wires: Vec<JsonWire>,
    cells: Vec<JsonCell>,
}

#[derive(Serialize)]
struct JsonPort {
    id: u32,
    dir: &'static str,
    name: String,
}

#[derive(Serialize)]
struct JsonWire {
    id: u32,
    name: String,
}

#[derive(Serialize)]
struct JsonCell {
    id: u32,
    name: String,
    type_: String,
    pins: Vec<u32>,
}

fn build_json(design: &Design) -> JsonDesign {
    let iface = design.iface();
    let ports = iface
        .ports()
        .map(|p| JsonPort {
            id: p.id,
            dir: match p.dir {
                PortDir::In => "input",
                PortDir::Out => "output",
            },
            name: p.name.clone(),
        })
        .collect();
    let wires = design
        .wire_list
        .iter()
        .map(|w| JsonWire {
            id: w.id,
            name: w.name.clone(),
        })
        .collect();
    let cells = design
        .cell_list
        .iter()
        .enumerate()
        .map(|(idx, c)| JsonCell {
            id: idx.try_into().unwrap(),
            name: c.name.clone(),
            type_: c.module.name().into(),
            pins: c.conn.iter().map(|x| x.1).collect(),
        })
        .collect();
    JsonDesign {
        name: iface.name().into(),
        ports,
        wires,
        cells,
    }
}

pub fn emit_json(store: &Store) -> String {
    let mut buf = Vec::<u8>::new();
    let mut ser = serde_json::Serializer::new(&mut buf);
    let top_design = store.modules().find(|m| !m.iface().attr_is_lib());
    let json_design = build_json(top_design.unwrap());
    serde_json::to_string_pretty(&json_design).unwrap()
}
