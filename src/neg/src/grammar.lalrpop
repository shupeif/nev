use super::TokenKind as TK;
use super::grammar_helper::*;
use super::grammar_helper::WithBuilder;

grammar<'input, 'db>(
    input: &'input str,
    db: &'db Builder,
);

pub Top: &'db Top<'db> = {
    <item_list: List<PkgItem>> => Top{<>}.with(db),
}

PkgItem: PkgItem<'db> = {
    Module => PkgItem::Module(<>),
}

Module: &'db Module<'db> = {
    <attr: Attr?>
    "module" <name: Ident>
    <port_list: ("(" <ListComma<Ident>> ")")?>
    "{" <stmt_list: List<ModItem>> "}"
    => Module{<>}.with(db),
}

ModItem: ModItem<'db> = {
    SigDecl => ModItem::SigDecl(<>),
    CellStmt => ModItem::Cell(<>),
    AssignStmt => ModItem::Assign(<>),
}

SigDecl: &'db SigDecl<'db> = {
    <kind: SigKind>  <vars: ListComma<Ident>> ";" => {
        SigDecl{<>}.with(db)
    }
}

AssignStmt: &'db AssignStmt<'db> = {
    <lhs: Ident> ":=" <rhs: Ident> ";" => {
        AssignStmt{<>}.with(db)
    }
}

CellStmt: &'db Cell<'db> = {
    "cell" <name: Ident> "=" <module_name: Ident>
    "(" <conn_list: ListComma<CellConn>> ")" ";" => {
        Cell{<>}.with(db)
    }
}

CellConn: &'db CellConn<'db> = {
    <pin:Ident> <dir:CellConnDir> <wire:Ident> => CellConn{<>}.with(db)
}

#[inline]
CellConnDir: PortDir = {
    ":<" => PortDir::In,
    ":>" => PortDir::Out,
}

SigKind: SigKind = {
    "input" => SigKind::Input,
    "output" => SigKind::Output,
    "wire" => SigKind::Wire,
}

#[inline]
ListCommaTuple<T>: PVec<'db, T> = {
    CommaTuple<T> => db.alloc_slice(&<>),
}

// used in tuple syntax
CommaTuple<T>: Vec<T> = {
    <vs: (<T> ",")+> => vs,
    <vs: (<T> ",")+> <v: T> => vec_push(vs, v),
}

#[inline]
ListComma<T>: PVec<'db, T> = {
    Comma<T> => db.alloc_slice(&<>),
}

Comma<T>: Vec<T> = {
    <vs: (<T> ",")*> => vs,
    <vs: (<T> ",")*> <v: T> => vec_push(vs, v),
}

#[inline]
List<T>: PVec<'db, T> = {
    T* => db.alloc_slice(&<>),
}

Attr: &'db Attr<'db> = "#" "[" <attrs: ListComma<AttrSeg>> "]" => Attr{<>}.with(db);

AttrSeg: &'db AttrSeg<'db> = {
    <name: Ident> <value: ("=" <Ident>)?> => AttrSeg{<>}.with(db),
}

StringLit: StringLit<'db> = start_double_quote <l: @L> <x: string_seg?> <r: @R> end_double_quote => db.mk_lit_string(&input[l..r], l..r);
Number: IntLit<'db> = <l: @L> <x: number> <r: @R> => db.mk_lit_number(&input[l..r]);
Ident: Ident<'db> = <l: @L> <x: ident> <r: @R> => db.mk_ident(&input[l..r], l..r);

extern {
    type Location = usize;
    type Error = std::convert::Infallible;

    enum TK {
        "(" => TK::LeftParen,
        ")" => TK::RightParen,
        "[" => TK::LeftBracket,
        "]" => TK::RightBracket,
        "{" => TK::LeftBrace,
        "}" => TK::RightBrace,
        "<" => TK::LeftAngle,
        ">" => TK::RightAngle,
        "," => TK::Comma,
        ";" => TK::Semicolon,
        ":" => TK::Colon,
        "." => TK::Dot,
        "*" => TK::Star,
        "#" => TK::Hash,

        start_double_quote => TK::DoubleQuoteStart,
        end_double_quote => TK::DoubleQuoteEnd,
        string_seg => TK::LitStringSeg,
        

        number => TK::Number,
        ident => TK::Ident,

        bit0 => TK::Bit0,
        bit1 => TK::Bit1,

        "=" => TK::Equal,
        ":=" => TK::Assign,
        ":<" => TK::ColonLeftAngle,
        ":>" => TK::ColonRightAngle,

        "module" => TK::KwModule,
        "input" => TK::KwInput,
        "output" => TK::KwOutput,
        "wire" => TK::KwWire,
        "cell" => TK::KwCell,
        "let" => TK::KwLet,
        "tuple" => TK::KwTuple,
    }
}