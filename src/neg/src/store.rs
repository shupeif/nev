use std::cell::Cell;
use std::rc::Rc;

#[derive(Clone, Copy, PartialEq, Eq, Hash, Debug)]
pub enum PortDir {
    In,
    Out,
}

pub type SignalId = u32;

#[derive(Debug, Clone)]
pub struct PortDef {
    pub id: SignalId,
    pub name: String,
    pub dir: PortDir,
}

#[derive(Debug)]
pub struct DesignAttr {
    pub is_lib: bool,
}

impl Default for DesignAttr {
    fn default() -> Self {
        DesignAttr { is_lib: false }
    }
}

#[derive(Debug)]
pub struct DesignInterface {
    name: String,
    attrs: DesignAttr,
    port_list: Vec<PortDef>,
}

impl DesignInterface {
    pub fn new(name: &str, attrs: DesignAttr) -> Self {
        DesignInterface {
            name: name.into(),
            attrs,
            port_list: vec![],
        }
    }
    pub fn name(&self) -> &str {
        &self.name
    }
    pub fn add_port(&mut self, name: &str, dir: PortDir) {
        let id = self._get_current_id();
        let name = name.into();
        self.port_list.push(PortDef { id, name, dir });
    }
    pub fn get_by_id(&self, id: SignalId) -> &PortDef {
        let def = &self.port_list[id as usize];
        debug_assert!(def.id == id);
        def
    }
    pub fn get_by_name(&self, name: &str) -> Option<&PortDef> {
        for def in &self.port_list {
            if def.name == name {
                return Some(def);
            }
        }

        None
    }
    pub fn ports(&self) -> impl Iterator<Item = &PortDef> + '_ {
        self.port_list.iter()
    }
}

impl DesignInterface {
    pub fn attr_is_lib(&self) -> bool {
        self.attrs.is_lib
    }
}

impl DesignInterface {
    fn _get_current_id(&self) -> SignalId {
        self.port_list.len().try_into().unwrap()
    }
}

#[derive(Debug)]
pub struct WireDef {
    pub id: SignalId,
    pub name: String,
}

#[derive(Debug)]
pub struct CellDef {
    pub module: Rc<DesignInterface>,
    pub name: String,
    pub conn: Vec<(SignalId, SignalId)>,
}

impl CellDef {
    pub fn new(module: Rc<DesignInterface>, name: &str) -> Self {
        CellDef {
            module,
            name: name.into(),
            conn: vec![],
        }
    }
    pub fn add_conn(&mut self, pin: &str, wire: SignalId) {
        let pin_id = self.module.get_by_name(pin).unwrap().id;
        self.conn.push((pin_id, wire));
    }
}

pub struct Design {
    iface: Rc<DesignInterface>,
    pub wire_list: Vec<WireDef>,
    pub cell_list: Vec<CellDef>,
    ctr_signal: Cell<u32>,
}

impl Design {
    pub fn new_with_iface(iface: Rc<DesignInterface>) -> Self {
        let ctr_signal = iface.port_list.len().try_into().unwrap();
        Design {
            iface,
            wire_list: vec![],
            cell_list: vec![],
            ctr_signal: Cell::new(ctr_signal),
        }
    }
    pub fn add_wire(&mut self, name: &str) {
        let id = self.ctr_signal.get();
        self.ctr_signal.set(id + 1);
        let name = name.into();
        self.wire_list.push(WireDef { id, name });
    }
    pub fn add_cell(&mut self, cell: CellDef) {
        self.cell_list.push(cell)
    }
    pub fn iface(&self) -> &DesignInterface {
        &self.iface
    }
    pub fn get_wire_by_name(&self, name: &str) -> Option<&WireDef> {
        self.wire_list.iter().find(|w| w.name == name)
    }
    pub fn get_signal_name_by_id(&self, id: SignalId) -> &str {
        assert!(id < self.ctr_signal.get());
        let port_count = self.iface.port_list.len();
        if (id as usize) < port_count {
            &self.iface.get_by_id(id).name
        } else {
            let wire_def = &self.wire_list[(id as usize) - port_count];
            debug_assert!(wire_def.id == id);
            &wire_def.name
        }
    }
    pub fn get_signal_id_by_name(&self, name: &str) -> Option<SignalId> {
        if let Some(port_def) = self.iface.get_by_name(name) {
            return Some(port_def.id);
        }
        if let Some(wire_def) = self.get_wire_by_name(name) {
            return Some(wire_def.id);
        }

        None
    }
}

pub struct Store {
    module_iface_list: Vec<Rc<DesignInterface>>,
    module_list: Vec<Design>,
    ctr: Cell<usize>,
}

impl Store {
    pub fn new() -> Self {
        Store {
            module_iface_list: vec![],
            module_list: vec![],
            ctr: Cell::new(0),
        }
    }
    pub fn add_module_iface(&mut self, iface: Rc<DesignInterface>) {
        self.module_iface_list.push(iface);
    }
    pub fn add_module(&mut self, module: Design) {
        self.module_list.push(module);
    }
    pub fn get_module_iface_by_name(&self, name: &str) -> Option<Rc<DesignInterface>> {
        self.module_iface_list
            .iter()
            .find(|m| m.name == name)
            .cloned()
    }

    pub fn foreach_module(&self, mut f: impl FnMut(&Design)) {
        for module in &self.module_list {
            f(module)
        }
    }
    pub fn modules(&self) -> impl Iterator<Item = &Design> + '_ {
        self.module_list.iter()
    }
}
