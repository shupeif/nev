pub mod intern;

#[macro_export]
macro_rules! nev_error {
    ($($arg:tt)+) => {
        std::panic!("compile error: {}", std::format_args!($($arg)+))
    };
}
